from pyluc import LUC

# dictionary defining a classification step 
# e.g. {'input class or value': 'output class', etc....}
# if the input or output class is a string (word) then use quotes around it 
#   (ie 'class_1')
# if the input or output class is a number then DON'T use quotes
#   (ie 15)
lcdb_preclass_lut = {
    47: 'nz_scrub',
    50: 'nz_scrub',
    52: 'nz_scrub',
    54: 'nz_scrub',
    55: 'nz_scrub',
    58: 'nz_scrub',
    69: 'nz_indig',
}

# a more complicated classification step, *inputs* contains attributes for all 
# input files and any output from classification steps run prior to this one, 
# accessed by inputs.*nice_name*. inputs.default always exists automatically and 
# is used to set all the values to a certain class. ORDER IS IMPORTANT, 
# subsequent classes will override any previous ones
def classify_publicprivate(inputs):
    return [
        ('Unclassed', inputs.default),
        ('Public', inputs.property_title.id == 0),
        ('Private', inputs.property_title.id > 0)
    ]

def classify_private_forestscrub(inputs):
    
    private_forest = [
        ('Unclassed', inputs.default),
        
        ('Private native forest', (inputs.publicprivate == 'Private') & (inputs.lcdb == 'nz_indig')),

        ('Public native forest', (inputs.publicprivate == 'Public') & (inputs.lcdb == 'nz_indig')),
         
        ('Private native scrub/grassland', (inputs.publicprivate == 'Private') & (inputs.lcdb == 'nz_scrub')),

        ('Public native scrub/grassland', (inputs.publicprivate == 'Public') & (inputs.lcdb == 'nz_scrub'))
    ]
    
    return private_forest

# initialise the framework for running this classification (and name it)
name = 'PrivateForest'
extent_nztm = 1835600, 5532700, 1857600, 5565800 # 1083000, 6207000, 2096000, 4745000
resolution_m = 100

# this line is required AFTER name, extent_nztm, resolution_m have been set
classification = LUC.Classification(name, extent_nztm, resolution_m)

# add information about the organisations and people that contributed to, and 
# own, this LUC. First, add an 'owner' organization, then 'other' organizations,
# then one or more LUC 'authors', then 'other' people. NOTE: the user who runs
# this script will be automatically added based on their login information, 
# affiliation is assumed to be the LUC owner, it is assumed that all LUC authors
# delegate authority to execute the LUC - to stop this, 'add_other_person' with
# a *shortname* that matches the username

# add_luc_owner(*shortname*, *name*, *url*)
#   *shortname* - a shortened lowercase name with no spaces or special 
#                    characters (acronyms work well i.e. "lr")
#   *name*      - full name of organization (i.e. "Landcare Research")
#   *url*       - organisation URL (i.e. 'http://www.landcareresearch.co.nz/')
classification.doc.add_luc_owner(
    'mw', 
    'Manaaki Whenua - Landcare Research', 
    'http://www.landcareresearch.co.nz/'
)

# add_other_organization(*shortname*, *name*, *url*)  <- see 'add_luc_owner'
classification.doc.add_other_organization(
    'linz',
    'Land Information New Zealand',
    'http://www.linz.govt.nz/'    
)

# add_luc_author(*shortname*, *fullname*, *affiliation*)
#   *shortname* - a shortened lowercase name with no spaces or special 
#                    characters (usernames work well i.e. "jollyb")
#   *fullname*  - full name of person (i.e. "Ben Jolly")
#   *affiliation*  - shortname of affiliated organisation (i.e. "lr")
classification.doc.add_luc_author(
    'ben.jolly',
    'Ben Jolly',
    'mw'
)

# add_other_person(*shortname*, *fullname*, *affiliation*, *delegators*)
# see 'add_luc_author'
#   *delegators* - an optional list of *shortnames* of people who delegate 
#                    authority to this person (i.e. to execute the LUC process)
classification.doc.add_luc_operator(
    'jollyb',#'ben.jolly',
    'Ben Jolly',
    'mw',
    delegators=['ben.jolly']
)

# add_other_person(*shortname*, *fullname*, *affiliation*, *delegators*)
# see 'add_luc_author'
#   *delegators* - an optional list of *shortnames* of people who delegate 
#                    authority to this person (i.e. to execute the LUC process)
classification.doc.add_other_person(
    'herziga',
    'Alex Herzig',
    'mw',
    delegators=['ben.jolly']
)

# add the input datasets
# arguments passed are (*nice_name*, *variable*, *LRIS_URL*)
#   *nice_name* is the name used to refer to data from this file later on
#   *variable* is the name of the variable to read from the input file (col name)
#   *LRIS_URL* is the link to the LRIS dataset to retrieve
classification.add_input_layer(
    'lcdb_raw', 
    'https://lris.scinfo.org.nz/layer/104400',
    attributed_to='mw'
)

# a 'local' version of an input layer (discouraged)
classification.add_input_layer(
    'property_title', 
    'data/lds-nz-property-titles-SHP.geojson',
    attributed_to='linz'
)
## Hosted version 'property_title' layer above (uncomment to swap)
#classification.add_input_layer(
#    'property_title', 
#    'https://lris.scinfo.org.nz/layer/460-publicprivatenzland01b/',
#    attributed_to='linz'
#)

#add classification steps
#arguments pass are (*author*, *nice_name*, *method*, *input* (optional))
#   *author*    - the *shortname* of the author responsible for this rule
#                   (an organization is acceptable if exact author unknown)
#   *nice_name* - the name used to refer to the output from this step in 
#                   subsequent steps
#   *method*    - the method to use, it can be a simple lookup table or a more 
#       complication 'function'
#   *input*     - the (optional) *nice_name* of a previous class step OR a data 
#       file, it is used when providing a simple lookup table so we know which 
#       dataset to apply it to
classification.add_classification_step('jollyb', 'lcdb', lcdb_preclass_lut, 'lcdb_raw', 'Class_2018')
classification.add_classification_step('jollyb', 'publicprivate', classify_publicprivate)
classification.add_classification_step('jollyb', 'Final', classify_private_forestscrub)
