"""
    An EXAMPLE script with FAKE API keys/authentication data

    NOTE: VERY IMPORTANT - when editing this to add your own keys, DO NOT put
    this in any kind of version control/sharing system - API keys/credentials
    often let you access sensitive or access-controlled data that should be
    protected from random people on the internet
"""

from pyluc import LUCAuth

my_auth = LUCAuth('User Name')

my_auth.add_ksite('lris.scinfo.org.nz', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
my_auth.add_ksite('data.linz.govt.nz', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')

my_auth.add_provstore('username', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')

my_auth.add_ftp('ftp.mysite.com', user='user.name', password='password123')
