# Datasets provided for example classifications
## Land Information New Zealand
### NZ Property Titles (lds-nz-property-titles-SHP.geojson)
 - Obtained on March 9th, 2020 from: https://data.linz.govt.nz/layer/50804-nz-property-titles/
 - Licensed under Creative Commons Attribution 4.0 International - https://creativecommons.org/licenses/by/4.0/
 - Approx. extent (NZTM - EPSG:2193): 1835635, 5532730, 1857593, 5565740