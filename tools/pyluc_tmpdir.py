#!/usr/bin/env python

import os
import shutil
import argparse
import glob

from zipfile import ZipFile

from pyluc import utils
from pyluc import LUC

parser = argparse.ArgumentParser()
parser.add_argument('tempdir')
parser.add_argument('originals', nargs='+')
parser.add_argument('--autodetect', action="store_true")
parser.add_argument('--flattendirs', action="store_true")
parser.add_argument('--reverse', action="store_true")
parser.add_argument('--reverseoriginals', action="store_true")
parser.add_argument('--reverseexternals', action="store_true")
parser.add_argument('--reversefiles', nargs='+', default=None)
parser.add_argument('--recursivereverse', action="store_true")
parser.add_argument('--quiet', action="store_true")
parser.add_argument('--dryrun', action="store_true")
args = parser.parse_args()

assert os.path.exists(args.tempdir) and os.path.isdir(args.tempdir), "Invalid directory specified! " + args.tempdir

args.reverse |= args.recursivereverse
    

do_reverse = args.reverse or \
    args.reverseoriginals or \
    (args.reversefiles is not None)

tocopy = []
copyto = []
destination = None
orig_dir = os.getcwd()

layer_zips = None
compressed_toignore = []

if args.autodetect:
    print('Auto-detecting layer zip files to copy...')
    for fn in args.originals:
        if fn.endswith('.py'):

            print('\tScanning {0} for LUC.Classification instance...'.format(fn))
            luc_classification, varname = utils.find_instance_in_script(LUC.Classification, fn)
            
            if luc_classification is not None:
                print('\tFound {0}, getting filenames...'.format(varname))
                layer_zips = luc_classification.get_input_files()
                print('\t', len(layer_zips))
                args.originals += layer_zips

                # should only have a single script file to deal with
                break
            else:
                print('\tNone found, continuing...')
    
    assert layer_zips is not None, '--autodetect found nothing!'
    
    for compressed in [x for x in args.originals if x.endswith('.zip')]:
        dirname = os.path.dirname(compressed)

        with ZipFile(compressed, 'r') as compressed_handle:
            compressed_toignore += [os.path.join(dirname, x) for x in compressed_handle.namelist()]

if do_reverse:
    
    destination = orig_dir
        
    if args.reversefiles is None:
        tocopy = glob.glob(os.path.join(args.tempdir, '*.*'))
        if args.recursivereverse:
            tocopy += glob.glob(os.path.join(args.tempdir, '*/*.*'))
        tocopy = [os.path.relpath(x, start=args.tempdir) for x in tocopy if os.path.isfile(x)]
    else:
        for fn in args.reversefiles:
            if os.path.isdir(fn):
                dir_contents = glob.glob(os.path.join(args.tempdir, fn, '*'))
                tocopy += [os.path.relpath(x, start=args.tempdir) for x in dir_contents if os.path.isfile(x)]
                if not args.quiet:
                    print('WARNING:', fn, 'is a directory, getting contents...')
                    
            else:
                tocopy.append(fn)

    toremove = []
    for fn in tocopy:
        if fn in args.originals + compressed_toignore:
            if not args.reverseoriginals:
                toremove.append(fn)
        else:
            if args.reverseoriginals and (not args.reverse) and (args.reversefiles is None or (fn not in args.reversefiles)):
                toremove.append(fn)
    
    for fn in toremove:
        tocopy.remove(fn)
        
    if not args.quiet:
        print('Reverse copy - destination = ' + destination)
else:
    tocopy = args.originals
    destination = args.tempdir
    
    if not args.quiet:
        print('Forward copy - destination = ' + destination)
        
        
if args.flattendirs:
    copyto = [os.path.join(destination, os.path.basename(x)) for x in tocopy]
else:
    copyto = [os.path.join(destination, x) for x in tocopy]

if do_reverse:
    tocopy = [os.path.join(args.tempdir, x) for x in tocopy]

assert len(tocopy) > 0, "No files to copy!"
assert destination is not None

for i in range(len(tocopy)):
    src, dst = tocopy[i], copyto[i]
    
    if not args.quiet:
        print(src, '->', dst)
    if not args.dryrun:
        if not os.path.exists(src):
            print('File {0} does not exist, skipping!'.format(src))
            continue

        directory = os.path.dirname(dst)
        if not os.path.exists(directory):
            print('Directory {0} does not exist, creating...'.format(directory))
            os.makedirs(directory)
        shutil.copy2(src, dst)
