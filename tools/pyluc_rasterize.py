#!/usr/bin/env python

import os
import argparse
import sys
import shutil
import re 
from datetime import datetime

from pathlib import Path

from osgeo import ogr

import pyproj

from rios import rat

import numpy as np

from pyluc import utils, RIOSEngine, Exceptions

logger, info, dbg, warn = utils.config_logger('pyluc_rasterize')

def ceil(x):
    return int(np.ceil(x))


def order(a, b):
    return min(a, b), max(a, b)


parser = argparse.ArgumentParser()
parser.add_argument('-in',    required=True,    dest='vecin',   type=Path)
parser.add_argument('-out',   required=True,    dest='rastout', type=Path)
parser.add_argument('-layer', required=False,                                           help='Specific layer of vector file to use')
parser.add_argument('-vky',   required=False,   nargs='+',              default=[],     help='Vector file keys to extract for RAT')
parser.add_argument('-rky',   required=False,   nargs='+',              default=None,   help='RAT names for -vky keys (defaults to -vky keys)')
parser.add_argument('-res',   required=False,               type=float, default=0)
parser.add_argument('-ext',   required=False,   nargs=4,    type=float, default=[0, 0, 0, 0], help='Extent (in a_srs) of area to rasterize')
parser.add_argument('-tsize', required=False,   nargs=2,    type=int,   default=[0, 0], help='*sizex sizey* of tile')
parser.add_argument('-tolap', required=False,               type=int,   default=None,   help='overlap for tile (in map units)')
parser.add_argument('-tidx',  required=False,               type=int,   default=-1,     help='tile index where tidx = x + y * sizex')
parser.add_argument('-lck',   required=False,                           default=None)
parser.add_argument('--all-attr', dest='all',   action='store_true',    default=False,  help='Add all attributes from vector file to RAT')

parser.add_argument('-a_srs', default='epsg:2193')
parser.add_argument('-a_nodata', default='0')


args = parser.parse_args()

vec_file = None

do_RAT = args.all or len(args.vky) > 0

if args.all and len(args.vky) > 0:
    raise Exception("If specifying --all-attr, you can't also supply vky")

if args.rky is None and do_RAT:
    args.rky = args.vky.copy()

if do_RAT:
    assert len(args.vky) == len(args.rky), "rky length must match vky length"

do_rasterize = args.res > 0

def get_file_layer_name_crs(vecin):
    #os.environ['SHAPE_RESTORE_SHX'] = 'YES'
    vec_file = ogr.Open(utils.safe_as_posix(vecin), 0)

    if args.layer is not None:
        vec_file_layer = vec_file.GetLayer(args.layer)
        vec_file_layer_name = args.layer
    else:
        vec_file_layer = vec_file.GetLayer()
        vec_file_layer_name = vec_file_layer.GetName()

    info(f"{args.layer} {vec_file_layer_name}")

    vec_file_layer_crs = vec_file_layer.GetSpatialRef().ExportToProj4()

    return vec_file, vec_file_layer, vec_file_layer_name, vec_file_layer_crs

vec_file, vec_file_layer, args.layer, vec_file_layer_crs = get_file_layer_name_crs(args.vecin)


proj_vector = pyproj.Proj(vec_file_layer_crs)
proj_raster = pyproj.Proj(args.a_srs)

if sum([abs(x) for x in args.ext]) == 0:
    args.ext = vec_file_layer.GetExtent()

    if not proj_vector.is_exact_same(proj_raster):
        args.ext = pyproj.transform(proj_vector, proj_raster, args.ext[:2], args.ext[2:])
        args.ext = [args.ext[1][0], args.ext[0][0], args.ext[1][1], args.ext[0][1]]

    dbg(f"Set extent automatically: {args.ext}")

# reproject input if needed
crs_similar, not_in_raster, not_in_vector = utils.proj_is_similar_enough(proj_vector, proj_raster)
if not crs_similar:
    reproj = args.vecin.parent / (args.layer + '_REPROJ.gpkg')
    dbg(f"Reprojecting {args.vecin} ({reproj})")
    cmd = f"""ogr2ogr -f "GPKG" -sql 'SELECT * FROM "{args.layer}"' -t_srs {args.a_srs} {reproj} {args.vecin}"""
    utils.run(cmd, get_std=False, fail_on_returncode=True)
    args.layer = None
    args.vecin = reproj
    vec_file, vec_file_layer, args.layer, vec_file_layer_crs = get_file_layer_name_crs(args.vecin)
else:
    dbg(f"{args.vecin} and {args.rastout} have similar projections:")
    dbg(f"{not_in_raster} - {not_in_vector}")

args.GDAL_dtype = 'UInt32'
args.FID = '(FID+0)'

if do_RAT:
    dbg('Pre-populating RAT...')

    # utils.run("gdalsetthematic.py " + args.rastout, get_std=False, fail_on_returncode=True)

    vec_file_layer_def = vec_file_layer.GetLayerDefn()
    vec_attr_keys = [vec_file_layer_def.GetFieldDefn(i).GetName() for i in range(vec_file_layer_def.GetFieldCount())]

    # Allowing for 'clip' layers in shapefiles means that users sometimes don't care about a given attribute (they just want the polys)
    # The easiest way to deal with this in pyluc is to send 'FID+1' as a key name so remove any instances of this PROVIDED there's no
    # column by this name in the shapefile
    fid = "FID+1"
    if fid in args.vky and fid not in vec_attr_keys:

        while fid in args.vky:
            i = args.vky.index(fid)
            args.vky.pop(i)
            args.rky.pop(i)
    
    if args.all:
        args.vky = vec_attr_keys
        args.rky = [re.sub(r'[^a-zA-Z\d_]', '_', attr_key) for attr_key in vec_attr_keys]
        dbg("Automatically populated vky and rky")
        dbg(f"vky: {args.vky}")
        dbg(f"rky: {args.rky}")

    fid_column = []
    vec_rat_lookup = dict((args.vky[i], args.rky[i]) for i in range(len(args.vky)))
    rat_type_lookup = None
    rat_columns = dict((att, []) for att in args.rky)

    for feature in vec_file_layer:
        fid_column.append(feature.GetFID())
        items = feature.items()
        for vec_key, rat_key in vec_rat_lookup.items():
            val = items[vec_key]
            if type(val) in [str, np.str_]:

                # try to keep string values to a reasonable length
                if len(val) > 255:
                    val=val[:255]

                # encode in utf-8 to save much pain later 
                val = val.encode('utf-8')

            rat_columns[rat_key].append(val)   

    min_fid = min(fid_column)
    if min_fid < 0:
        raise Exception()
    elif min_fid == 0:
        args.FID = '(FID+1)'
        fid_column = [x+1 for x in fid_column]
    elif min_fid > 1:
        args.FID = f'(FID-{min_fid-1})'
        fid_column = [x-(min_fid-1) for x in fid_column]
        
    dbg(f"min(FID) == {min_fid}, PYLUCID = '{args.FID}'")
    min_fid, max_fid = min(fid_column), max(fid_column)
    nrows = len(fid_column)#rat_columns[args.rky[0]])

    if max_fid < 2**8:
        args.GDAL_dtype = 'Byte'
    elif max_fid < 2**16:
        args.GDAL_dtype = 'UInt16'
    elif max_fid >= 2**32:
        args.GDAL_dtype = 'UInt64'


if do_rasterize:
    dbg('Starting rasterize...')

    args.ext[0], args.ext[2] = order(args.ext[0], args.ext[2])
    args.ext[1], args.ext[3] = order(args.ext[1], args.ext[3])

    if (args.tidx >= 0):

        assert args.tsize[0] * args.tsize[1] > 0, 'Invalid tile size'

        i = args.tidx
        sizex, sizey = args.tsize

        extx, exty = args.ext[2] - args.ext[0], args.ext[3] - args.ext[1]
        nx = ceil(extx / sizex)

        ix, iy = i % nx, i // nx

        off_x = ix * sizex
        off_y = iy * sizey

        new_lowerleft = args.ext[0] + off_x, args.ext[1] + off_y
        new_upperright = new_lowerleft[0] + sizex, new_lowerleft[1] + sizey

        if args.tolap is not None and args.tolap > 0:
            new_lowerleft = new_lowerleft[0] - args.tolap, new_lowerleft[1] - args.tolap
            new_upperright = new_upperright[0] + args.tolap, new_upperright[1] + args.tolap

        args.ext = max(args.ext[0], new_lowerleft[0]), max(args.ext[1], new_lowerleft[1]), min(args.ext[2], new_upperright[1]), min(args.ext[3], new_upperright[1])

        act_sizex, act_sizey = args.ext[2] - args.ext[0], args.ext[3] - args.ext[1]

        dbg('Rasterizing tile {0} {1} ({2} x {2}), BL corner {3} {4}'.format(ix, iy, act_sizex, act_sizey, args.ext[0], args.ext[1]))

    args.ext = ' '.join([str(x) for x in args.ext])

    args.fidrastout = '.fid'.join(os.path.splitext(args.rastout)) if do_RAT else args.rastout
    args.SELECT = 'SELECT ' + ('GEOM,' if args.vecin.suffix.lower() == '.gpkg' else '') + f' {args.FID} AS PYLUCID FROM "{args.layer}"'
    info(f"SQL='{args.SELECT}'")
    cmd = r"""gdal_rasterize \
        -of KEA \
        -tap \
        -tr {res} {res} \
        -te {ext} \
        -a PYLUCID \
        -sql '{SELECT}' \
        -ot {GDAL_dtype} \
        -a_srs {a_srs} \
        -a_nodata {a_nodata} \
        {vecin} \
        {fidrastout}""".format(**args.__dict__)

    utils.run(cmd, get_std=False, fail_on_returncode=True)
    dbg('Done')
else:
    dbg('Skipping raster creation (no args supplied)')

if do_RAT:
    #import cProfile
    #with cProfile.Profile() as pr:

    if len(args.vky) == 0 and not args.all:
        dbg('Stopped RAT creation as user only wanted FID+1 anyway (see pixel values for this)')
        shutil.copy2(args.fidrastout, args.rastout)
        utils.gdalsetthematic(args.rastout)
    else:
        
        str_rows = ['|'.join([str(rat_columns[rat_key][i]) for rat_key in args.rky]) for i in range(nrows)]
        uq_rows = list(set(str_rows))
        
        uq_rat_columns = None
        # if we can reduce the number of entries in our RAT by at least 25% then do that now
        if len(uq_rows) <= len(str_rows) * 0.75:
            dbg('Optimising RAT entries ({1} -> {0})'.format(len(uq_rows), len(str_rows)))
            uq_rows.sort()
            uid_row = [uq_rows.index(str_row) + 1 for str_row in str_rows]
            uid_lut = np.zeros((max_fid + 1, ), dtype=np.uint32)
            uid_lut[fid_column] = uid_row
            RIOSEngine.process_lookup(args.fidrastout, args.rastout, uid_lut, 0, True)

            uq_rat_columns = dict((att, []) for att in args.rky)
            for uq_row in uq_rows:
                i_eg = str_rows.index(uq_row)
                for att in args.rky:
                    uq_rat_columns[att].append(rat_columns[att][i_eg])
        else:
            dbg('Skipping RAT optimisation')
            shutil.copy2(args.fidrastout, args.rastout)
            utils.gdalsetthematic(args.rastout)

        os.remove(args.fidrastout)

        for key in uq_rat_columns or rat_columns:
            #import pdb; pdb.set_trace()
            rat_data = rat_columns[key] if uq_rat_columns is None else uq_rat_columns[key]

            # it's possible to have multiple 'types' in one column when dealing with 'NULL' string
            #  values (do not enumerate as string or int when first value inspected)

            rat_type, rat_default, rat_data = utils.get_RAT_column_dtype(key, rat_data)

            dbg(f"{key} datatype = {rat_type}")

            array = None
            if uq_rat_columns is None:
                #array = np.zeros((max_fid + 1, ), dtype=rat_type)
                array = np.array([rat_default] + rat_data, dtype=rat_type)[[0] + fid_column]
            else:
                array = np.asarray([rat_default] + rat_data, dtype=rat_type)
            #import pdb; pdb.set_trace()
            dbg(f"{args.rastout} '{key}' {array[:4]}")
            rat.writeColumn(utils.safe_as_posix(args.rastout), key, array)
        dbg('Done')
        #pr.print_stats()
else:
    dbg('Skipping RAT creation (no args supplied)')

if args.lck is not None:
    if os.path.exists(args.lck):
        os.remove(args.lck)
    else:
        dbg('Expected to find lock file \'{0}\' but it was gone! Rasterize/RATerize successful anyway...'.format(args.lck))

dbg('Done')
sys.exit(0)
