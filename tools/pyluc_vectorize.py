#!/usr/bin/env python

import os
import argparse
import sys

import numpy as np

from rios import rat

from osgeo import ogr

from pyluc import utils

parser = argparse.ArgumentParser()
parser.add_argument('raster')
#parser.add_argument('attribute')
parser.add_argument('vector')
parser.add_argument('-f', dest='format', default='ESRI Shapefile')
parser.add_argument('-b', dest='band', default=1)

args = parser.parse_args()

gpkg_file_driver = ogr.GetDriverByName(args.format)

if os.path.exists(args.vector):
    gpkg_file_driver.DeleteDataSource(args.vector)

#cmd = "gdal_polygonize.py {keafile} -mask {keafile} -8 -f \"ESRI Shapefile\" -b {band} {shapefile}".format(**args.__dict__)
cmd = f"""gdal_polygonize.py {args.raster} -mask {args.raster} -8 -f "{args.format}" -b {args.band} {args.vector}"""

utils.run(cmd, get_std=False, fail_on_returncode=True)

print('Initialising vector attribute table')
gpkg_file = gpkg_file_driver.Open(args.vector, 1)      # 0 == readonly
gpkg_file_layer = gpkg_file.GetLayer()
rat_cols = rat.getColumnNames(args.raster)
rat = dict(
    (col, rat.readColumn(args.raster, col)) for col in rat_cols
)
type_in = lambda dtype, types: np.any([np.issubdtype(x, dtype) for x in types])
nfeat = None
intcols, floatcols, strcols = [], [], []
for column in rat_cols:
    if nfeat is None:
        nfeat = len(rat[column])

    ogr_type = None
    if np.issubdtype(rat[column].dtype, np.string_):
        ogr_type = ogr.OFTString
        strcols.append(column)
    elif np.issubdtype(rat[column].dtype, np.float_):
        ogr_type = ogr.OFTReal
        floatcols.append(column)
    elif np.issubdtype(rat[column].dtype, np.signedinteger) or np.issubdtype(rat[column].dtype, np.unsignedinteger):
        ogr_type = ogr.OFTInteger if rat[column][0].nbytes < 8 else ogr.OFTInteger64
        intcols.append(column)
    else:
        raise Exception(f"Unknown type {rat[column].dtype}")

    new_field = ogr.FieldDefn(column, ogr_type)
    gpkg_file_layer.CreateField(new_field)

#lut = rat.readColumn(args.raster, args.attribute).astype('U')
print(f'Copying class names from RAT into "{args.format}"...')

for i, feature in enumerate(gpkg_file_layer):
    for column in intcols:
        feature.SetField(column, int(rat[column][feature.GetField(0)]))

    for column in floatcols:
        feature.SetField(column, float(rat[column][feature.GetField(0)]))

    for column in strcols:
        feature.SetField(column, rat[column][feature.GetField(0)].decode('utf-8'))
    
    gpkg_file_layer.SetFeature(feature)
    
    if i % int(nfeat // 100) == 0:
        print(f"{i/nfeat*100:3.0f}% ({i:8d} features of {nfeat:8d})")
    del feature
    
gpkg_file.Destroy()

print('All done')

sys.exit(0)
