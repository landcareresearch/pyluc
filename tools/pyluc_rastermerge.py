#!/usr/bin/env python

import argparse

from collections import OrderedDict

import numpy as np


from rios import applier
from rios import rat

from pyluc import utils

parser = argparse.ArgumentParser()
parser.add_argument('output')
parser.add_argument('attribute')
parser.add_argument('inputs', nargs='+')
parser.add_argument('--norat', action='store_true')
args = parser.parse_args()

args.inputs_ssv = ' '.join(args.inputs)


def apply_ratidx_fix(info, infiles, outfiles, otherargs):
    input_data = otherargs.lut[infiles.data]
    outfiles.result = np.zeros_like(infiles.data)
    for i, key in enumerate(otherargs.master_lut):
        outfiles.result[input_data == key] = i


master_lut = None
input_luts = dict()
bad_inputs = []
if not args.norat:
    longest = 0
    for fn in args.inputs:
        try:
            lut = rat.readColumn(fn, args.attribute)
        except Exception as ex:
            bad_inputs.append(fn)
            print("Error accessing {0} - file may be corrupt - message: {1}".format(fn, ex.message))
            continue

        if lut is not None:
            if type(lut) is list:
                lut = np.asarray(lut, dtype='<U')
            elif type(lut) is np.ndarray:
                if lut.dtype.char == 'S':
                    # need our string arrays to be unicode otherwise comparisons don't work properly later on
                    lut = lut.astype('U')
                    
        # make sure master_lut starts out as being the longets of the luts (less
        # chance of something missing) - important for maintaining a reasonable
        # degree of order
        if len(lut) > longest:
            master_lut = list(lut)
            
        input_luts[fn] = lut
    
    for bad_input in bad_inputs:
        args.inputs.remove(bad_input)
        
    # we need to determine if any classes are missing from our master list
    for lut in input_luts.values():
        diffs = set(lut) - set(master_lut)
        if len(diffs) > 0:
            # this is a bit complicated but maintains some semblance of order
            master_lut += [x for x in list(lut) if x in diffs]
    
    # master_lut should contain only unique values (need to maintain order so
    # can't use 'set()')
    master_lut = np.asarray(list(OrderedDict.fromkeys(master_lut)))

    colortable = rat.genColorTable_gray(len(master_lut))

    ratidx_fix = False
    for fn in args.inputs:
        ratidx_fix |= len(input_luts) != len(master_lut)

    if ratidx_fix:
        otherargs = applier.OtherInputs()
        otherargs.master_lut = master_lut
        
        for fn in args.inputs:
            infiles = applier.FilenameAssociations()
            infiles.data = fn
            
            outfiles = applier.FilenameAssociations()
            outfiles.result = fn
            
            controls = applier.ApplierControls()
            controls.referenceImage = fn
            controls.thematic = True
            controls.calcStats = True
            controls.drivername = 'KEA'
    
            otherargs.lut = input_luts[fn]
            
            applier.apply(apply_ratidx_fix, infiles, outfiles, otherargs, controls)
            
            rat.writeColumn(fn, args.attribute, master_lut)
            rat.setColorTable(fn, colortable)

utils.run('gdal_merge.py -of KEA -o {output} {inputs_ssv}'.format(**args.__dict__), get_std=False, fail_on_returncode=True)

if not args.norat:
    
    utils.gdalsetthematic(args.output)

    rat.writeColumn(args.output, args.attribute, master_lut)
    rat.setColorTable(args.output, colortable)

utils.run('gdalcalcstats ' + args.output, get_std=False, fail_on_returncode=True)
