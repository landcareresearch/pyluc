#!/usr/bin/env python

import os
import sys
import argparse
import logging 

from pathlib import Path

import time

from pyluc import LUC
from pyluc import LUCAuth
from pyluc import utils
from pyluc import Exceptions

logger, info, dbg, warn = utils.config_logger('pyluc_process', level=logging.INFO)

states = ['acquire', 'process', 'merge', 'prov']
special_states = ['previous']

DEFAULT_STATE = states[0]

parser = argparse.ArgumentParser()
parser.add_argument('script',       type=Path,                              help="Path to pyluc script to process")
parser.add_argument('--state',                      default=DEFAULT_STATE,  help="Resume from specific processing state")
parser.add_argument('--stage',      type=int,       default=0,              help="Start at this 'stage' of LUC step processing (default 0)")
parser.add_argument('--idx',                        default=None,           help="Tile index")
parser.add_argument('--istep',                      default=None,           help="Start at step index 'istep'")
parser.add_argument('--uploadprov',                 action="store_true",    help="Upload PROV data automatically")
parser.add_argument('-f',           dest='force',   action="store_true",    help="Force reprocessing")
parser.add_argument('-v',           dest='verbose', action="store_true",    help="Verbose logging"    )

args = parser.parse_args()

if args.verbose:
    logger.setLevel(logging.DEBUG)

root = Path('.').absolute()
info(f'Appending project root {root} to PATH...')
sys.path.append(root.as_posix())

assert args.script.exists(), f"Could not find script {args.script}"
assert str(args.script.parent) == '.', 'Script MUST be located in CWD'

# if args.tempdir is not None:
#     assert os.path.exists(args.tempdir), 'Temp directory does not exist'.format(args.tempdir)
    
if args.idx is not None:
    args.idx = int(args.idx)

info('Auto-detecting LUC.Classification instance...')
luc_classification, varname = utils.find_instance_in_script(LUC.Classification, args.script)
assert luc_classification is not None, 'Expected to find instance of LUC.Classification in script but it wasn\'t there'
info(f"Found {varname}, using it...")

luc_classification.set_loglevel(logging.DEBUG if args.verbose else logging.INFO)

authfile, authdata = args.script.parent / 'auth.py', None
if authfile.exists():
    authdata, varname = utils.find_instance_in_script(LUCAuth.Auth, authfile)
    if authdata is not None:
        info(f"Found {authfile}, using {varname} for auth data")

if authdata is None:
    authfile = authfile.with_suffix('.json')
    if authfile.exists():
        authdata = LUCAuth(json_file=authfile)
        info(f"Found {authfile}, using it for auth data")
    else:
        info("WARNING: No auth file found!")

luc_classification.set_auth_data(authdata)

if args.state not in states:
    if args.state == special_states[0]:
        info('Attempting to load state from file...')
        args.state = luc_classification._proc_steps.get_previous_step()
        
    if args.state not in special_states:
        raise Exceptions.LUCInvalidStateError('Invalid state: {0}'.format(args.state))

args.additionalargs = '-f ' if args.force else ''

info(f'State = {args.state}')

if args.state == states[0]:
    
    done = False
    while not done:
        info('Checking download status of input data...')
        done = luc_classification._proc_steps.acquire_input_data(args.force)
        if not done:
            waitfor = 60
            info(f'\tNot done, retrying in {waitfor} seconds...')
            time.sleep(waitfor)
        else:
            info('\tDone\n')
            args.state = states[1]
            info(f'State = {args.state}')
        
if args.state == states[1]:
    info('Processing input data (rasterize/resample then classify)...')
    assert luc_classification._proc_steps.process_input_data(args.force, args.idx, args.stage), "Unknown problem processing classification"
    info('\tDone\n')
    args.state = states[2]
    info(f'State = {args.state}')

if args.state == states[2]:
    info('Merging output data and vectorizing...')
    assert luc_classification._proc_steps.merge_results(args.force, args.idx), "Unknown problem merging classification results"
    info('\tDone\n')
    args.state = states[3]
    info(f'State = {args.state}')
    
if args.state == states[3]:
    info('Generating LaTeX documentation and saving...')
    #luc_classification.generate_latex_documentation()
    
    info('Generating provenance{0}data...'.format(' and uploading ' if args.uploadprov else ' '))
    luc_classification.generate_provenance(upload=args.uploadprov)
    
    info('\tDone\n')
else:
    raise Exceptions.LUCInvalidStateError()

sys.exit(0)
