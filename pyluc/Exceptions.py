"""
Created on Mon Apr 11 16:17:54 2016

@author: jollyb
"""

from pathlib import Path

class LUCError(Exception):
    pass


class LUCKError(LUCError):
    pass


class LUCRasterizeError(LUCError):
    pass


class LUCFileNotFoundError(LUCError):
    pass


class LUCUnexpectedDataTypeError(LUCError):
    pass


class LUCInvalidStateError(LUCError):
    pass


class LUCCmdExecutionError(LUCError):
    pass
   

def assert_path_exists(path):
    if type(path) is not Path:
        path = Path(path)
    if not path.exists():
        raise LUCFileNotFoundError(path)
