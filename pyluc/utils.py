"""
Created on Wed Jun 15 09:19:44 2016

@author: jollyb
"""
import os
import sys
import re
import json
from enum import Enum

import logging

from datetime import datetime, timedelta

import pathlib
from pathlib import Path
from filelock import FileLock

import numpy as np

from subprocess import PIPE
from subprocess import Popen

import inspect

import runpy
from osgeo import gdal

from . import Exceptions

if sys.platform == "linux" or sys.platform == "linux2":
    # linux
    import pwd
elif sys.platform == "darwin":
    # OS X
    pass
elif sys.platform == "win32":
    # Windows
    import ctypes

def config_logger(name, level=logging.DEBUG):

    logger = logging.getLogger(name)
    logger.setLevel(level)
    
    # create file handler which logs even debug messages
    # fh = logging.FileHandler('spam.log')
    # fh.setLevel(logging.DEBUG)
    
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(level)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    # logger.addHandler(fh)
    logger.addHandler(ch)

    logger.info(f'Logger Configured for {name}')

    return logger, lambda x: logger.info(x), lambda x: logger.debug(x), lambda x: logger.warning(x)
    

logger, info, dbg, warn = config_logger('pyluc.utils')

class Info(object):
    
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
    
    def __len__(self):
        return len(self.__dict__)
    
    def __iter__(self):
        return iter(self.__dict__)
        
    def __getitem__(self, key):
        return self.__dict__[key]
    
    def __setitem__(self, key, value):
        self.__dict__[key] = value
        
    def find_and_replace(self, input_str, prefix='{{', suffix='}}'):
        
        result = input_str
        
        for key, value in self.__dict__.items():
            result = result.replace(prefix + key + suffix, value)
            
        return result
        

class Workflow():

    class State:
        NULL = 0
        PROC = 1
        DONE = 2

    def __init__(self, steps, statefile, loadstate=False, loadmeta=False):

        self.steps = steps
        self.names = [name for name, method in self.steps]
        self.nsteps = len(self.steps)
        self.istep = 0

        # this is cumbersome but if we one-line self.meta we can end up with multiple
        # copies of the same dict instead of multiple dicts
        self.states, self.meta = {}, {}
        for name in self.names:
            self.states[name] = Workflow.State.NULL
            self.meta[name] = {}

        self.statefile = statefile
        if type(self.statefile) is str:
            self.statefile = Path(self.statefile)
        self.statefilelock = FileLock(statefile.with_suffix('.lock'))

        if loadstate or loadmeta:
            self.load_state(loadstate,loadmeta,soft_fail=True)

    def process(self, steps=None, uptoincl=None, force=False):

        upper_bound = self.nsteps if uptoincl is None else self.names.index(uptoincl) + 1

        if steps is None:
            steps = self.names[self.istep:upper_bound]
        elif type(steps) is str:
            steps = [steps]
        
        self.istep = self.names.index(steps[0])

        step_chain = True
        #import pdb; pdb.set_trace()
        while self.istep < upper_bound:
            name, method = self.steps[self.istep]

            if name in steps:
                #print(name)
                if force or self.states[name] < Workflow.State.DONE:
                    #print('\tnot done')
                    if not step_chain:
                        raise Exceptions.LUCError("Workflow: We skipped an incomplete processing step somewhere")

                    self.states[name] = Workflow.State.PROC
                    result = method(self.meta, force)
                    if result is None:                  # special case - something went wrong so try to 'roll back'
                        #print('\t\tresult none')
                        self.states[name] = Workflow.State.NULL
                        self.istep = max(0, self.istep - 1)
                        step_chain = False
                        break
                    elif result:
                        #print('\t\tdone')

                        self.states[name] = Workflow.State.DONE
                        if name == steps[-1]:
                            # done so break
                            break 
                    else:
                        #print('\t\tstill going')
                        step_chain = False
                        break
                else:
                    #print('\talready completed')
                    if name == steps[-1]:
                        # done so break
                        break 
            
            #step_chain = step_chain and self.states[name] == Workflow.State.DONE
            self.istep += 1
            self.save_state()
        self.save_state()
        
        if self.istep >= len(self.steps):
            last_step = self.steps[-1][0]
            if self.states[last_step] != Workflow.State.DONE:
                #print(self.istep, last_step, self.states[last_step])
                raise Exceptions.LUCInvalidStateError("We've run out of steps but the last step isn't done")
            else:
                self.istep = len(self.steps) - 1

        self.save_state()
        return step_chain

    def complete(self, step=None):
        if step is None:
            step = self.names[-1]

        return self.states[step] == Workflow.State.DONE

    def status(self, step=None):
        istep = min(self.nsteps-1, self.istate)
        if step is None:
            step = self.names[istep]
        else:
            istep = self.names.index(step)
        
        return istep, step, self.states[step] == Workflow.State.DONE, self.states[step]


    def load_state(self, loadstate, loadmeta, soft_fail=False):
        if file_exists_withcontent(self.statefile):
            with self.statefilelock.acquire(timeout=10):
                with open(self.statefile, 'r') as handle:
                    self.set_state(handle.read(), isjson=True, metaonly=loadmeta and not loadstate)
        elif not soft_fail:
            raise Exceptions.LUCFileNotFoundError(f"{self.statefile} not found")


    def save_state(self):
        with self.statefilelock.acquire(timeout=10):
            with open(self.statefile, 'w') as handle:
                handle.write(self.get_state(tojson=True))

    def get_state(self, tojson=True):
        #print(self.istep, self.steps)
        result = {
                'current': {'stepname': self.steps[self.istep][0], 'stepindex': self.istep}, 
                'states': self.states, 
                'meta': self.meta
            }

        if tojson:
            result = json.dumps(result)

        return result

    def set_state(self, obj, isjson=True, metaonly=False):
        if isjson:
            obj = json.loads(obj)
        
        if obj['current']['stepname'] not in self.names:
            raise Exceptions.LUCInvalidStateError(f"JSON state info contains unknown state {obj['current']['stepname']}")

        if self.names.index(obj['current']['stepname']) != obj['current']['stepindex']:
            raise Exceptions.LUCInvalidStateError(f"JSON state info inconsistent with loaded states (bad index)")

        if not metaonly:
            self.istep = obj['current']['stepindex']
            self.states = obj['states']

        self.meta = obj['meta']

    def get_meta(self, step):

        return self.meta[step]

    def set_meta(self, step):

        return self.meta[step]

def file_exists_withcontent(filepath):
    return filepath.exists() and filepath.stat().st_size > 0

def enforce_Path(filename):
    return Path(filename) if type(filename) is str else filename

def safe_as_posix(path_or_list):
    if type(path_or_list) in [list, tuple]:
        path_or_list = [safe_as_posix(x) for x in path_or_list]

    return path_or_list.as_posix() if type(path_or_list) in [pathlib.PosixPath] else path_or_list

def safe_as_Path(str_or_list):
    if type(str_or_list) in [list, tuple]:
        str_or_list = [safe_as_Path(x) for x in str_or_list]

    return Path(str_or_list) if type(str_or_list) in [str] else str_or_list
       
def run(command, get_std=True, print_first=True, block=True, fail_on_returncode=False):
    
    if print_first:
        print(command)
        
    block |= get_std
    
    if not block and not get_std and fail_on_returncode:
        print('WARNING: Cannot fail on returncode if non-blocking run requested')
        
    process = Popen(
        args=command,
        stdout=PIPE if get_std else None,
        stderr=PIPE if get_std else None,
        shell=True
    )
    
    out, err, returncode = None, None, None
    
    if block:
        if get_std:
            out, err = process.communicate()
            out, err = out.decode('UTF-8').strip().split('\n') if out else out, err.decode('UTF-8').split('\n') if err else err
        else:
            process.wait()
            
        returncode = process.returncode
            
        if fail_on_returncode and returncode > 0:
            raise Exceptions.LUCCmdExecutionError('Error detected (code {0}) while executing:\n\t{1}\nStdOut:\n{2}\nStdErr:\n{3}'.format(returncode, command, out, err))
        
    return out, err, returncode
    

def find_instance_in_script(instance, script):
    namespace = runpy.run_path(str(script))

    for varname, var in namespace.items():
        if type(var) is instance:
            return var, varname
            
    return None, None
    

def proj_is_similar_enough(proja, projb):
    """Compare two pyproj.Proj objects

    For some reason, files that register as EPSG 2193 do not
    exactly match a 2193 Proj object so compare the PROJ4
    strings maually
    """

    def todict(prj):
        result = prj.crs.to_proj4().strip().split(' ')
        result = [x.split('=') for x in result]
        #info(f"{result}")
        result = dict((x[0], x[1] if len(x) == 2 else 'SET') for x in result)
        #info(f"{result}")
        return result
    
    a, b = todict(proja), todict(projb)
    anotb, bnota = [], []
    equal_ish = True
    for key, val in a.items():
        if key in b:
            equal_ish &= val == b[key]
        else:
            anotb.append(key)
    
    bnota = [key for key in b if key not in a]

    return equal_ish, anotb, bnota



def gdalsetthematic(fn):
    ds = gdal.Open(safe_as_posix(fn), gdal.GA_Update)
    for band in range(ds.RasterCount):
        bh = ds.GetRasterBand(band + 1)
        bh.SetMetadataItem('LAYER_TYPE', 'thematic')
    
    del ds


def get_RAT_column_dtype(name, rat_data):
    clean_data = [x for x in rat_data if x is not None]
    rat_type = list(set(map(type, clean_data)))
    if len(rat_type) == 0:
        rat_type = [str]
    

    info(f"get_RAT_column_dtype {name} {rat_type}")
    rat_default=None

    type_in = lambda dtype, types: np.any([np.issubdtype(x, dtype) for x in types])
    types_in = lambda dtypes, types: np.any([type_in(dtype, types) for dtype in dtypes])

    if type_in(np.str_, rat_type):
        rat_data = [str(x) if x is not None else '' for x in rat_data]
        rat_default = ''
        ct = max([len(s) for s in rat_data])
        rat_type = '<U' + str(ct)
    elif datetime in rat_type:
        rat_type = np.datetime64
        rat_default = np.datetime64('1970-01-01')
        rat_data = [x if x is not None else rat_default for x in rat_data]
    elif type_in(np.float_, rat_type):
        maxval = np.nanmax(np.abs(np.array(clean_data, dtype=np.float64)))
        # assume we need 6 digits of precision to 3 DP
        if maxval < 8192:
            rat_type = np.float32
        else:
            rat_type = np.float64

        rat_default = np.nan
    elif types_in([np.uint8, np.uint16, np.uint32, np.uint64], rat_type):
        maxval = np.nanmax(clean_data)

        if maxval < 2**8:
            rat_type = np.uint8
        elif maxval < 2**16:
            rat_type = np.uint16
        elif maxval < 2**32:
            rat_type = np.uint32
        else:
            rat_type = np.uint64

        rat_default = 0
    elif types_in([np.int8, np.int16, np.int32, np.int64], rat_type):
        minval, maxval = np.nanmin(clean_data), np.nanmax(np.abs(clean_data))
        uint = minval >= 0
        pow_mod = 1 if uint else 0

        if maxval < 2**(8-pow_mod):
            rat_type = np.uint8 if uint else np.int8
        elif maxval < 2**(16-pow_mod):
            rat_type = np.uint16 if uint else np.int16
        elif maxval < 2**(32-pow_mod):
            rat_type = np.uint32 if uint else np.int32
        else:
            rat_type = np.uint64 if uint else np.int64

        rat_default = 0
    elif bytes in rat_type:
        rat_data = ['' if x is None else x.decode('utf-8') for x in rat_data]
        # we've had issues with rogue characters getting in and screwing up the ascii process when writing to RAT so check all chars here
        rat_data = [''.join([c if ord(c) < 128 else '*' for c in x]) for x in rat_data]
        rat_default = ''
        ct = max([len(s) for s in rat_data])
        rat_type = '<U' + str(ct)
    elif types_in([bool, np.bool], rat_type):
        rat_data = [False if x is None else x for x in rat_data]
        rat_default = False
        rat_type = np.uint8
    else:
        raise Exceptions.LUCUnexpectedDataTypeError(f"Attribute has unknown datatype(s) {rat_type}")

    return rat_type, rat_default, rat_data

def get_user_info():
    """Get username, first name, and last name for current user
    
    Returns
    -------
    dict
        Dictionary with three keys: 'username', 'firstname', 'lastname'
    """
    username = None
    fullname = None
    if sys.platform == "linux" or sys.platform == "linux2":
        # linux
        username = os.getenv('USER')
        fullname = pwd.getpwuid(os.getuid())[4].split(',')[0]
        
    elif sys.platform == "darwin":
        # OS X
        raise Exceptions.LUCError("OS X not yet supported for user info")
    elif sys.platform == "win32":
        # Windows
        username = os.getenv('USERNAME')
        
        GetUserNameEx = ctypes.windll.secur32.GetUserNameExW
        NameDisplay = 3
    
        size = ctypes.pointer(ctypes.c_ulong(0))
        GetUserNameEx(NameDisplay, None, size)
    
        nameBuffer = ctypes.create_unicode_buffer(size.contents.value)
        GetUserNameEx(NameDisplay, nameBuffer, size)
        fullname = nameBuffer.value
        
    # splitname = re.split('\s+', fullname)
    # firstname = splitname[0] if len(splitname[0]) > 0 else None
    # lastname = splitname[-1] if len(splitname) > 1 else None
    
    return username, fullname


def get_calling_line(level=2):
    return inspect.stack()[level].code_context


def get_var_source(var, varidx, varname, level=4):
    
    calling_line = ''.join(get_calling_line(level=level))
    if type(var) in [dict, list, tuple]:
        var_source = str(var)
    else:
        try:
            var_source = ''.join(inspect.getsource(var))
        except Exception as ex:
            var_source = calling_line
            # this was undocumented so no idea what exception we're catching so raise it
            raise ex
    
    original_var_name, original_var_declaration = None, None

    args_start = calling_line.find('(') + 1
    bkts = 1
    args = []
    arg = ''
    lda = False
    for i, char in enumerate(calling_line):
        if i < args_start:
            continue

        bkts += 1 if char in '([]' else -1 if char in ')]' else 0
    
        if bkts:
            if char == ',' and bkts == 1 and not lda:
                args.append(arg.strip())
                arg = ''
            else:
                if char == 'l' and var_source[i:i + 6] == 'lambda':
                    lda = True
    
                if lda and char == ':':
                    lda = False
    
                arg += char
                
    args.append(arg.strip())
    
    for i, arg in enumerate(args):

        kwarg = re.findall(r'(\w+)\s*=.*', arg)

        if len(kwarg) == 1 and varname == kwarg[0]:

            original_var_declaration = arg[arg.find('=') + 1:].strip()
            original_var_name = original_var_declaration
    
    if original_var_declaration is None:
        if varidx >= len(args):
            original_var_declaration = None
        else:
            original_var_declaration = args[varidx]
        original_var_name = original_var_declaration
    
    # if var was declared before it was given to fn
    if var_source != calling_line:
        original_var_declaration = var_source
        
    return original_var_name, original_var_declaration
        

def find_lambdas_in_function_call(code):
    matches = None
    if 'lambda' in code:
        matches = re.findall(r'((\w+\s*=\s*)?lambda [\w,\s]+:[^,(]+(\(.*\))?)[,)]', code)
        if len(matches) > 0:
            matches = [match[0] for match in matches]
    
    return matches

