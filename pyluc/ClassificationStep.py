# -*- coding: utf-8 -*-
"""
Created on Mon Apr 11 09:51:22 2016

@author: jollyb
"""


from collections import OrderedDict
from . import Exceptions
from . import utils

import numpy as np


class ClassificationStep(object):
    """
    Base class for a single step of a landuse classification. This should be
    subclassed, NOT used as an object its own right.

    Attributes
    ----------
    name : str
        Name to call the resulting dataset so it can accessed later.
    """

    def __init__(self, name, classify_replacement=None, skip_save=False, isthematic=False):
        """Create LUT classification step

        Parameters
        ----------
        name : str
            Name to call the results so they can be accessed by later steps.
        classify_replacement : callable, optional
            callable matching ClassificationStep.classify function description.
            
        Returns
        -------
        ClassificationStep
            A classification step.
            
        """
        
        self.name = name
        self.isthematic = isthematic
        self._classify = classify_replacement
        #self.class_list = []
        self.dtype = None

        self.skip_save = skip_save
        
        self.tile_suffix = ''
        
    def classify(self, info, infiles, outfiles, otherargs):
        """Classify some input data.
        
        Designed to be called from rios.applier.apply(). Should be overridden
        by subclass or optionally via __init__ (not recommended)
        
        Parameters
        ----------
        info : rios.readerinfo.ReaderInfo
        infiles : rios.applier.FilenameAssociations
        outfiles : rios.applier.FilenameAssociations
        otherargs : rios.applier.OtherInputs
    
        Returns
        -------
        np.ndarray
            two-dimensional array of classifications, outfiles is not touched
            (it is up to the caller to manage that).
        
        See Also
        --------
        rios.applier.apply()
        """
        if self._classify is None:
            raise NotImplementedError()
        else:
            return self._classify(info, infiles, outfiles, otherargs)
            
    #def get_class_list(self):
    #    return self.class_list
        
            
class ComplexMethodClassificationStep(ClassificationStep):
    def __init__(self, name, classify_method, continuous=False, class_list=None, skip_save=False, isthematic=False):
        """Create classification step using a simplified python function/lambda
                
        Parameters
        ----------
        name : str
            Name to call the results so they can be accessed by later steps.
        classify_method : callable
            Function or lambda that accepts one arg (infiles)
            
        Returns
        -------
        ComplexMethodClassificationStep
            A subclass of ClassificationStep.
                        
        Raises
        ------
        LUCUnexpectedDataTypeError
            If classify is not callable
        
        See Also
        --------
        LUTClassificationStep : For simple classifications using only a LUT
        """
        
        if not hasattr(classify_method, '__call__'):
            raise Exceptions.LUCUnexpectedDataTypeError()
            
        self._method = classify_method
        #self.class_list = class_list
        super().__init__(name, skip_save=skip_save, isthematic=isthematic)
        
    def classify(self, info, infiles, outfiles, otherargs):
        """Classify some input data.
        
        Designed to be called from rios.applier.apply().
        
        Parameters
        ----------
        info : rios.readerinfo.ReaderInfo
        infiles : rios.applier.FilenameAssociations
        outfiles : rios.applier.FilenameAssociations
        otherargs : rios.applier.OtherInputs
    
        Returns
        -------
        np.ndarray
            two-dimensional array of classifications, outfiles is not touched
            (it is up to the caller to manage that)
        
        See Also
        --------
        rios.applier.apply()
        """
        
        return self._method(info, infiles, outfiles, otherargs)
        

class LUTClassificationStep(ClassificationStep):
    """
    A single step of a landuse classification that uses a simple lookup table
    """
    def __init__(self, name, LUT, input_layer, attribute, skip_save=False, isthematic=False, default_val=None):
        """Create LUT classification step
                
        Parameters
        ----------
        name : str
            Name to call the results so they can be accessed by later steps.
        input_name : str
            Name of the input dataset to use (must match a defined dataset).
        LUT : list(tuple) OR dict
            Either a list of tuples - eg [('class', input_val)] - OR a 'dict'.
            
        Returns
        -------
        LUTClassificationStep
            A subclass of ClassificationStep.
                        
        Raises
        ------
        LUCUnexpectedDataTypeError
            If the LUT passed in can't be deciphered
        
        See Also
        --------
        SimpleMethodClassificationStep : For more complicated classifications
        """
        
        self._input_layer = input_layer

        if attribute is None:
            self._attribute = input_layer
        else:
            self._attribute = attribute
        
        try:
            if type(LUT) is list:
                if type(LUT[0]) is not tuple:
                    LUT = [tuple(entry) for entry in LUT]
            elif type(LUT) in [dict, OrderedDict]:
                LUT = list(LUT.items())
            else:
                raise Exceptions.LUCError()
        except Exceptions.LUCError:
            raise Exceptions.LUCUnexpectedDataTypeError(
                'LUT type ({0}) unexpected, expected list of tuples or a dict'.format(type(LUT))
            )
        
        self._LUT = LUT

        self.default_val = default_val
           
        super().__init__(name, skip_save=skip_save, isthematic=isthematic)
        
    def classify(self, info, infiles, outfiles, otherargs=None):
        """Classify some input data.
        
        Designed to be called from rios.applier.apply().
        
        Parameters
        ----------
        info : rios.readerinfo.ReaderInfo
        infiles : rios.applier.FilenameAssociations
        outfiles : rios.applier.FilenameAssociations
        otherargs : rios.applier.OtherInputs
    
        Returns
        -------
        np.ndarray
            two-dimensional array of classifications, outfiles is not touched
            (it is up to the caller to manage that)
        
        See Also
        --------
        rios.applier.apply()
        """
        
        input_data = getattr(infiles, self._input_layer)
        if self._attribute is not None:
            input_data = getattr(input_data, self._attribute)

        if self.dtype is None:
            if type(self._LUT[0][1]) in [str]:
                maxlen = max([len(x[1]) for x in self._LUT])
                self.dtype = f"<U{maxlen}"
            else:
                self.dtype = type(self._LUT[0][1])
            
        result = np.zeros(input_data.shape, dtype=self.dtype)
        if self.default_val is None:
            if self.dtype in [float, np.float32, np.float64]:
                self.default_val = np.nan
            elif self.dtype in [int, np.int8, np.int16, np.int32, np.int64, np.uint8, np.uint16, np.uint32, np.uint64]:
                self.default_val = 0
                
        result[:] = self.default_val
        
        is_bulk_lut = hasattr(self._LUT[0][0], '__call__')
        
        #if len(self.class_list) == 0:
        #    self.class_list = [class_out for selector, class_out in self._LUT]

        for selector, class_out in self._LUT:
            if is_bulk_lut:
                result[selector(input_data)] = class_out
            else:
                result[input_data == selector] = class_out

        return result
        
        
class SimpleMethodClassificationStep(ClassificationStep):
    
    def __init__(self, name, classify_method, skip_save=False, isthematic=False, default_val=None, wholescene=False):
        """Create classification step using a simplified python function/lambda
                
        Parameters
        ----------
        name : str
            Name to call the results so they can be accessed by later steps.
        classify_method : callable
            Function or lambda that accepts one arg (infiles)
            
        Returns
        -------
        SimpleMethodClassificationStep
            A subclass of ClassificationStep.
                        
        Raises
        ------
        LUCUnexpectedDataTypeError
            If classify is not callable
        
        See Also
        --------
        LUTClassificationStep : For simple classifications using only a LUT
        """
        
        if not hasattr(classify_method, '__call__'):
            raise Exceptions.LUCUnexpectedDataTypeError()
            
        self._method = classify_method

        self.default = default_val

        self.wholescene = wholescene

        super().__init__(name, skip_save=skip_save, isthematic=isthematic)
        
    def classify(self, info, infiles, outfiles, otherargs):
        """Classify some input data.
        
        Designed to be called from rios.applier.apply().
        
        Parameters
        ----------
        info : rios.readerinfo.ReaderInfo
        infiles : rios.applier.FilenameAssociations
        outfiles : rios.applier.FilenameAssociations
        otherargs : rios.applier.OtherInputs
    
        Returns
        -------
        np.ndarray
            two-dimensional array of classifications, outfiles is not touched
            (it is up to the caller to manage that)
        
        See Also
        --------
        rios.applier.apply()
        """
        
        classifier = self._method(infiles)

        result = None
        
        if type(classifier) is np.ndarray or self.wholescene:
            result = classifier
        else:
            kvpairs = classifier.items() if type(classifier) in [dict, OrderedDict] else classifier
    
            if self.dtype is None:
                classes = [class_out for class_out, selector in kvpairs]
                self.dtype, self.default, nul = utils.get_RAT_column_dtype(self.name + '.CS', classes)

            result = np.zeros(infiles.default.shape, dtype=self.dtype)
            result[:] = self.default
            
            for class_out, selector in kvpairs:
                result[selector] = class_out
        
        return result

class CopyAttributeClassificationStep(ClassificationStep):
    
    def __init__(self, name, input_layer, attribute, isthematic=False):
        """Copy an attribute from an input layer
                
        Parameters
        ----------
        name : str
            Name to call the results so they can be accessed by later steps.
        input_layer : str
            Name of input layer to use
        attribute : str
            Name of attribute of input layer to copy

        Returns
        -------
        SimpleMethodClassificationStep
            A subclass of ClassificationStep.
        
        See Also
        --------
        LUTClassificationStep : For simple classifications using only a LUT
        """
        
        self.input_layer = input_layer
        self.attribute = attribute
        
        super().__init__(name, skip_save=False, isthematic=isthematic)
        
    def classify(self, info, infiles, outfiles, otherargs):
        """Classify some input data.
        
        Designed to be called from rios.applier.apply().
        
        Parameters
        ----------
        info : rios.readerinfo.ReaderInfo
        infiles : rios.applier.FilenameAssociations
        outfiles : rios.applier.FilenameAssociations
        otherargs : rios.applier.OtherInputs
    
        Returns
        -------
        np.ndarray
            two-dimensional array of classifications, outfiles is not touched
            (it is up to the caller to manage that)
        
        See Also
        --------
        rios.applier.apply()
        """
        
        return getattr(getattr(infiles, self.input_layer), self.attribute)