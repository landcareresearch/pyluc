# -*- coding: utf-8 -*-
"""
Created on Mon Apr 11 09:50:02 2016

@author: jollyb
"""


"""
Default docstring examples based on numpy docs readme:
(https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt)


class Photo(ndarray):
    ""
    Array with associated photographic information.

    ...

    Attributes
    ----------
    exposure : float
        Exposure in seconds.

    Methods (NOTE: not normally required but can be specified like this)
    -------
    colorspace(c='rgb')
        Represent the photo in the given colorspace.
    gamma(n=1.0)
        Change the photo's gamma exposure.

    ""


def add(a, b, method='A'):
   ""The sum of two numbers.

    A more detailed description of how the two numbers are summed.
    
    Parameters
    ----------
    a, b : numeric
        Numbers to sum, should be actual numbers.
    method : {'A', 'B', 'C'}, optional
        Could be one of three strings.
        
    
    Returns
    -------
    int
        Description of anonymous integer return value.
        
    
    Raises
    ------
    NotImplementedError
        If the person writing the code hasn't actually written most of it yet.
    
    See Also
    --------
    subtract : A function to subtract two numbers.
   ""

"""

pass
