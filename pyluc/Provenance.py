"""
Created on Fri Apr 21 09:20:42 2017

@author: jollyb
"""
import re
from datetime import datetime

from provstore.api import Api as ProvStoreApi
from prov.model import ProvDocument, PROV

from . import utils, Exceptions

known_hosts = {
    'lris.scinfo.org.nz': 'lris',
    'data.linz.govt.nz': 'data.linz'
}


def _prefix(namespace, name):
    return ':'.join([namespace if type(namespace) is str else namespace.prefix, name])

# _get_obj = lambda from_attr, str_or_obj: from_attr[str_or_obj] if type(str_or_obj) is str else str_or_obj


def _get_list(obj):
    return obj if type(obj) is list else [obj]


def _get_obj(from_attr, str_or_obj):
    if type(str_or_obj) is str:
        for attr in _get_list(from_attr):
            if str_or_obj in attr:
                return attr[str_or_obj]
    else:
        return str_or_obj

    return None

  
class LUCProvenance(object):
    def __init__(self, name, auth=None):
        """Create a object to handle provenance for a LUC
        
        This handles all prov information generation and allows automated
        upload to ProvStore. Note that all prov information must be added in
        the following order:
            Organizations (all) -> People (all) -> Layers (all) -> Rules (all)
        before calling 'finalize()' to tidy a few loose ends up
        
        Parameters
        ----------
        name : str
            Name of the LUC
        auth : LUCAuth (optional)
            Object with two attributes - 'user' and 'api_key' - containing
            auth info for the ProvStore website
        
        Returns
        -------
        LUCProvenance
            An object for managing LUC prov information
        
        See Also
        --------
        Intro to PROV: http://www.w3.org/TR/2013/NOTE-prov-primer-20130430/
        Underlying library: http://prov.readthedocs.io/en/1.5.0/
        """

        self.ns = utils.Info()      # NameSpaces
        self.en_ly = utils.Info()   # Entities (LRIS Layers)
        self.en_in = utils.Info()   # Entities (Inputs)
        self.en_res = utils.Info()  # Entities (Results)
        self.ag = utils.Info()      # Agents
        self.ac = utils.Info()      # Activities (LUC Rules)
        
        self.res_generated_by = []
        
        self.luc_owner = []
        self.luc_author = []
        self.luc_operator = None
        
        self.final_result = None
        self.finalized = False
        
        self.luc_name = name
        self.doc = ProvDocument()
        
        self.ns.prov = self.doc.add_namespace('prov', 'http://www.w3.org/ns/prov#')
        self.ns.foaf = self.doc.add_namespace('foaf', 'http://xmlns.com/foaf/0.1/')
        
        # the following will be set when an 'owner' organisation is added
        self.ns.rules = None
        self.ns.inputs = None
        self.ns.results = None
        
        # this will be set if and when a 'clip' layer is added
        self.ac.clip = None
        
        self.auth = auth
     
    def add_organization(self, shortname, name, url, is_luc_owner=False, attr=dict()):
        """Add an organization 'Agent', this should be the first step
        
        This will set up a 'Namespace' and 'Agent' for an organisation
        affiliated with this LUC or its inputs. The Agent is prefixed by the
        new Namespace. If this organization is the luc_owner then three extra
        Namespaces are created: 'rules', 'inputs', and 'results'.
        The URL pattern for these is:
            [organisation_url]/lucs/[luc_name]/[rules|inputs|results]
            
        Parameters
        ----------
        shortname : str
            Short name (i.e. acronym) for organisation (i.e. 'lr'). Lowercase
            alphanumeric characters only (no spaces)
        name : str
            Full name of organization, can include any UTF-8 characters
        url : str
            URL to organization's website (preferably). URL does not need to
            actually exist but must be unique to this organisation and follow
            basic URL convention
        is_luc_owner : bool (optional)
            If True then tag this organization as the owner of the LUC and set
            up rules/input/results namespaces as appropriate
        attr : dict (optional)
            Any extra attributes as required (see W3C PROV documentation)
        
        Returns
        -------
        tuple(prov.identifier.Namespace, prov.model.ProvAgent)
            The resulting namespace and agent objects created
            
        See Also
        --------
        https://www.w3.org/TR/2013/REC-prov-o-20130430/#Agent
        """
        
        if shortname in self.ns or shortname in self.ag:
            raise Exceptions.LUCError('Organization\'s name must be unique')
        
        attr[PROV["type"]] = PROV["Organization"]
        attr['foaf:name'] = name

        self.ns[shortname] = self.doc.add_namespace(shortname, url)
        self.ag[shortname] = self.doc.agent(_prefix(shortname, shortname), attr)
        
        if is_luc_owner:
            self.luc_owner.append(self.ns[shortname])
            for luc_ns in ['rules', 'inputs', 'results']:
                self.ns[luc_ns] = self.doc.add_namespace(luc_ns, '{0}/lucs/{1}/{2}/'.format(url.strip('/'), self.luc_name, luc_ns))

        return self.ns[shortname], self.ag[shortname]
        
    def add_person(self, shortname, fullname, affiliation, is_luc_author=False, is_luc_operator=None, delegators=None, attr=dict()):
        """Add a person 'Agent' to prov info. Add people AFTER organizations
        
        This will set up an 'Agent' for each person affiliated with this LUC.
        These people should be used when specifying authors for LUC rules. An
        author for the overall LUC is flagged using 'is_luc_author' - there
        MUST be at least one of these for every LUC (can be more). An operator
        (computer user who ran the LUC) is flagged using the 'is_luc_operator'
        flag - there MUST be one AND ONLY one of these per LUC. If a person is
        operating under the authority of one or more others (must be 'added'
        prior) then the 'delegators' should be included. 'delegators' can also
        be used to identify secondary organization affiliations if required.
            
        Parameters
        ----------
        shortname : str
            Short name for person, usually first name or username, must be
            unique, alphanumeric chars
            only (incl '_')
        fullname : str
            Full name of person, can include any UTF-8 characters
        affiliation : str OR prov.model.ProvAgent
            Person's primary organizational affiliation (organization must have
            already been added). If using a string then specify the 'shortname'
            used to add the organization
        is_luc_author : bool (optional)
            If True then tag this person as an author of the entire LUC
        is_luc_operator : bool (optional)
            If True then tag this person as the one who actually ran the LUC on
            their computer (or one under their control). Only one of these can
            be specified - it is perferable to use their computer username for
            'shortname'
        delegators : str OR prov.model.ProvAgent OR list() (optional)
            One or more (prior-defined) people who delegated authority to take
            actions (such as run the LUC) to the person being added. Normally
            used when the one running the LUC is not the primary author
        attr : dict (optional)
            Any extra attributes as required (see W3C PROV documentation)
        
        Returns
        -------
        prov.model.ProvAgent
            The resulting namespace and agent objects created
            
        See Also
        --------
        https://www.w3.org/TR/2013/REC-prov-o-20130430/#Agent
        """
        if shortname in self.ag:
            raise Exceptions.LUCError('Actor\'s name must be unique')
        
        attr[PROV["type"]] = PROV["Person"]
        attr['foaf:name'] = fullname
        
        affiliation = _get_obj(self.ag, affiliation)
            
        self.ag[shortname] = self.doc.agent(_prefix(affiliation.identifier.namespace, shortname), attr)
        self.ag[shortname].actedOnBehalfOf(affiliation)
        
        if delegators is not None:
            for delegator in _get_list(delegators):
                self.ag[shortname].actedOnBehalfOf(_get_obj(self.ag, delegator))
            
        if is_luc_author:
            self.luc_author.append(self.ag[shortname])
            
        if is_luc_operator:
            if self.luc_operator is not None:
                raise Exceptions.LUCError('Can only have one LUC Operator (should be current user)')
            self.luc_operator = self.ag[shortname]
            
        return self.ag[shortname]
            
    def add_input_layer(self, name, url, attributed_to=None, clip_layer=False):
        """Add an input layer 'Entity' to prov info. Add layers AFTER people
        
        This method is designed to work on LRIS/Koordinates Layer URLs and
        performs several steps:
            1 - identify base path from URL (string up to final component) and
                add this is a namespace if one does not already exist (assume
                possible multiple layers from a single hostname)
            2 - identify the name of the 'parent layer' (final component of
                URL) and add this as an 'Entity'.
            3 - add an 'Entity' for the pyluc input layer
            4 - if this is a 'clip' layer, find or generate the 'clip' rule
                then add this layer to its list of entities
            
            NOTE that when using multiple fields/attributes from an input
            dataset each is specified as a unique layer in pyluc with its own
            name. To represent this properly in PROV we add an Entity for the
            'parent layer' (input dataset), then an Entity for each pyluc
            'layer' derived from itand specify this relationship using
            'wasDerivedFrom'
        
            
        Parameters
        ----------
        name : str
            Name of layer - lowercase alphanumeric characters only (no spaces)
        field : str
            Name of field/attribute extracted from parent layer
        url : str
            URL to layer (must conform to LRIS/Koordinates conventions), MUST
            resolve
        attributed_to : str OR prov.model.ProvAgent (optional)
            Prior-defined Agents (people and/or organizations) to which the
            PARENT LAYER should be attributed
        clip_layer : bool (optional)
            If this layer is used to clip outputs then set this to 'True', a
            clipping rule (Activity) will automatically be created and this
            layer will be assigned to it
        
        Returns
        -------
        tuple(prov.identifier.ProvEntity, prov.model.ProvEntity)
            The resulting parent layer (either created or retrieved) and input
            layer
            
        See Also
        --------
        https://www.w3.org/TR/2013/REC-prov-o-20130430/#Entity
        """
        if name in self.en_in:
            raise Exceptions.LUCError('Input\'s name must be unique')
            
        if self.luc_operator is None:
            raise Exceptions.LUCError('Operator MUST be specified before calling this')
        
        url_split = re.split('/+', url.strip('/'))
        if url_split[0].endswith(':'):
            hostname = url_split[1]
        else:
            hostname = url_split[0]
            
        if hostname in known_hosts:
            namespace = known_hosts[hostname]
        else:
            namespace = hostname.replace('www.', '')
            
        parent_layername = url_split[-1]
        url_base = '/'.join(url_split[:-1]) + '/'
            
        if namespace not in self.ns:
            self.ns[namespace] = self.doc.add_namespace(namespace, url_base)
            
        if parent_layername not in self.en_ly:
            self.en_ly[parent_layername] = self.doc.entity(_prefix(namespace, parent_layername))
            
            if attributed_to is not None:
                for agent in _get_list(attributed_to):
                    self.en_ly[parent_layername].wasAttributedTo(_get_obj(self.ag, agent))
            
        self.en_in[name] = self.doc.entity(_prefix(namespace, name))#, {self.ns.foaf['name']: field})
        self.en_in[name].wasDerivedFrom(self.en_ly[parent_layername])
        
        if clip_layer:
            if self.ac.clip is None:
                self.ac.clip = self.doc.activity(self.ns.rules['auto_clip'])
                self.ac.clip.wasAssociatedWith(self.luc_operator, attributes={PROV['role']: 'operator'})
            
            self.ac.clip.used(self.en_in[name])
            
        return self.en_ly[parent_layername], self.en_in[name]
            
    def add_rule(self, step_name, function_name, uses_inputs, authors):
        """MUST BE ADDED IN ORDER. Add rules AFTER layers"""
        
        if self.ns.rules is None:
            raise Exceptions.LUCError('You must add a \'owner\' organization BEFORE adding rules')
            
        if step_name in self.en_res:
            raise Exceptions.LUCError('Step\'s name ({0}) must be unique'.format(step_name))
            
        if function_name in self.ac:
            raise Exceptions.LUCError('Function\'s name ({0}) must be unique'.format(function_name))
            
        if authors is None:
            raise Exceptions.LUCError('Must specify at least one author for a given rule')
              
        if self.luc_operator is None:
            raise Exceptions.LUCError('Operator MUST be specified before calling this')
            
        self.ac[function_name] = self.doc.activity(self.ns.rules[function_name])
        self.ac[function_name].wasAssociatedWith(self.luc_operator, attributes={PROV['role']: 'operator'})
        
        for ipt in _get_list(uses_inputs):
            self.ac[function_name].used(_get_obj([self.en_in, self.en_res], ipt))
            
        self.en_res[step_name] = self.doc.entity(self.ns.results[step_name])

        # save this relationship in a tuple so we can define it later with a timestamp
        self.res_generated_by.append((self.en_res[step_name], self.ac[function_name]))
        
        if authors is not None:
            for author in _get_list(authors):
                author = _get_obj(self.ag, author)
                self.ac[function_name].wasAssociatedWith(author, attributes={PROV['role']: 'author'})
                self.en_res[step_name].wasAttributedTo(author)

        self.final_result = self.en_res[step_name]
        return self.ac[function_name], self.en_res[step_name]
    
    def finalize(self):
        """Don't call this until all rules have been added, but only call it once"""
        
        if self.finalized:
            raise Exceptions.LUCError('finalize() should only be called once')
            
        now = datetime.now()
        for en_res, ac in self.res_generated_by:
            en_res.wasGeneratedBy(ac, now)
          
        if self.ac.clip is not None:
            self.final_result.wasGeneratedBy(self.ac.clip, now)
            
        self.finalized = True
        
    def get_prov_string(self, prov_format='provn'):
        """Serialize the PROV document to produce a string to save or upload
        
        Will automatically call finalize() if this hasn't been done yet
        
        Parameters
        ----------
        prov_format : str
            Any format accepted by prov.model.ProvDocument.serialize(), i.e.
            'json', 'rdf', 'provn', 'xml'
        """
        if not self.finalized:
            self.finalize()
            
        return self.doc.serialize(format=prov_format)
        
    def upload_to_provstore(self, public=False):
        
        prov_string = self.get_prov_string('json')
        prov_format = 'json'
            
        api = ProvStoreApi(username=self.auth.provstore.user, api_key=self.auth.provstore.api_key)
        
        self.provstore_doc = api.document.create(prov_string, prov_format=prov_format, name=self.luc_name, public=public)
        
        return self.provstore_doc.url
