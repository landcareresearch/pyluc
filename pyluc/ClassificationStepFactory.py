# -*- coding: utf-8 -*-
"""
Created on Mon Apr 11 09:52:21 2016

@author: jollyb
"""

from . import Exceptions
from . import ClassificationStep as steps


def create_classification_step(name, lut_or_method, input_name=None, attribute=None, isthematic=True, default_val=None, wholescene=False):
    """Return an appropriate subclass of ClassificationStep.

    This factory will decide which subclass of ClassificationStep to return
    based on the supplied optional input arguments.
    
    Parameters
    ----------
    name : str
        Name to call the resulting dataset so it can accessed later.
    lut_or_method : list of tuple OR dict OR callable
        A lookup table (list or dict) OR method that generates one of those.
    input_name : str, optional
        Name of the input dataset to use, REQUIRED if a 'list' or 'dict' lut is supplied.
        
    
    Returns
    -------
    ClassificationStep
        A classification step to be called from within rios.applier.apply().
        
    
    Raises
    ------
    LUCError
        If there is a problem with the parameters supplied.
    
    See Also
    --------
    ClassificationStep : Base class for a single step of a landuse classification.
    """
    
    if callable(lut_or_method):
        if attribute is not None:
            print("WARNING: attribute specified but not used by callable class. step")
        num_method_args = lut_or_method.__code__.co_argcount
        if num_method_args == 1:
            return steps.SimpleMethodClassificationStep(name, lut_or_method, isthematic=isthematic, default_val=default_val, wholescene=wholescene)
        elif num_method_args == 4:
            return steps.ClassificationStep(name, lut_or_method, isthematic=isthematic)
        else:
            raise Exceptions.LUCError('Couldn\'t decipher function parameters')
    else:

        if input_name is None:
            raise Exceptions.LUCError('input_name is required if lut is not callable')

        if lut_or_method is None:
            return steps.CopyAttributeClassificationStep(name, input_name, attribute, isthematic=isthematic)
        else:
            if attribute is None:
                input_name, attribute = 'pyluc_steps', input_name
            return steps.LUTClassificationStep(name, lut_or_method, input_name, attribute, isthematic=isthematic, default_val=default_val)
