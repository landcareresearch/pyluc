"""
Created on Mon Apr 18 16:44:37 2016

@author: jollyb
"""
import numpy as np
from scipy import ndimage

import logging

from osgeo import gdal

from collections import OrderedDict
from collections import defaultdict

from rios import applier
from rios import rat
from rios import fileinfo

import re

from . import utils

logger, info, dbg, warn = utils.config_logger('pyluc.RIOSEngine')

class Dummy(object):
    pass

class ThematicLayerData:

    def __init__(self, layer=None, is_input=False):

        self.__external_RAT_columns = []

        if type(layer) is str:
            self.__layer = Dummy()
            self.__layer.name = layer
        else:
            self.__layer = layer
            if is_input:
                self.__external_RAT_columns = rat.getColumnNames(utils.safe_as_posix(self.__layer.raster))


        self.__layer_attributes = OrderedDict(
            #(key, None) for key in preloaded_attributes
        )
        
        self.__RAT_columns = []
        self.__RAT_cache = defaultdict(lambda: [None])#defaultdict(list) 

        self.__all_classes = defaultdict(lambda: len(self.__all_classes)+1)
        self.__class_LUT = defaultdict(lambda : [None])
        self.__data = None
        self.__debug = 0

        self.__alternative_LayerData_sources = []

    def LayerData_attribute_in_external_RAT(self, attr):
        return attr in self.__external_RAT_columns

    def LayerData_add_alternative(self, thematiclayerdata):
        self.__alternative_LayerData_sources.append(thematiclayerdata)

    def LayerData_clear_alternatives(self, savemem):
        for alternative in self.__alternative_LayerData_sources:
            alternative.__data = None
            alternative.__layer_attributes = OrderedDict()
            
        self.__alternative_LayerData_sources = []

    # weird name helps to avoid clashes with attribute columns from RAT
    def LayerData_load_data(self, data, set_attribute=None):
        dbg(f"LayerData_load_data {self.__layer.name} {set_attribute}")

        if set_attribute is None:
            self.__data = data
            self.__layer_attributes = {}

        if set_attribute is not None:
            self.__layer_attributes[set_attribute] = data.copy()

    def LayerData_export_data(self, savemem=False):
        """Build raster with RAT representing all attributes of this layer

        Returns
        -------
        np.ndarray, list, dict
            3D array (single band raster), RAT column names, RAT data
        """
        dbg(f"LayerData_export_data() {self.__layer.name} - Start")
        for attr in self.__layer_attributes:
            if attr not in self.__RAT_columns:
                self.__RAT_columns.append(attr)
            if attr not in self.__class_LUT:
                dbg(f"Adding {attr} to self.__class_LUT")
                self.__class_LUT[attr] = defaultdict(lambda : None) 

        shape = self.__layer_attributes[self.__RAT_columns[0]].shape
        if len(shape) != 3 or shape[0] > 1:
            raise Exception(f"Invalid shape (expected single band raster, got {shape})")

        nlayers = len(self.__layer_attributes)        
        
        invshape = shape[1:] + (1,)
        layers = np.zeros(shape[1:] + (nlayers,), dtype=np.uint32)

        dbg(f"LayerData_export_data() {self.__layer.name} - {nlayers} layers, building classes...")
        two_step_detection = True
        # build list of classes that belong to each attribute
        for iattr, attr in enumerate(self.__RAT_columns):  

            uq_classes, uq_idxs = np.unique(self.__layer_attributes[attr], return_inverse=True)

            for uq_class in uq_classes:
                if self.__class_LUT[attr][uq_class] is None:
                    self.__class_LUT[attr][uq_class] = len(self.__class_LUT[attr])-1

            # if only one attr (layer) then we can stop here
            # if nlayers == 1:
            #     dbg(f"LayerData_export_data() {self.__layer.name} - Only 1 layer, copying it directly")
            #     layers = self.__layer_attributes[attr][0][:,:,np.newaxis]
            #     two_step_detection = False
            #     del(uq_classes, uq_idxs)
            #     break

            if savemem:
                self.__layer_attributes[attr] = None

            uq_idxs = uq_idxs.reshape(shape[1:])
            objects = ndimage.find_objects(uq_idxs+1)   # add 1 so all classes are 'found'

            #layer = np.zeros(shape, dtype=np.uint32)
            dbg(f"LayerData_export_data() {self.__layer.name} - Found {len(uq_classes)} classes ({len(objects)} objects) for {attr}")

            for i, slice in enumerate(objects):
                sel = uq_idxs[slice] == i

                #layer[slice][sel] = self.__class_LUT[attr][uq_classes[i]]
                layers[:,:,iattr][slice][sel] = self.__class_LUT[attr][uq_classes[i]]

            del(uq_classes,uq_idxs,objects)
        
        layerdtype = layers.dtype
        if layers.dtype in [np.uint8, np.int8]:
            nbytes = 1
        elif layers.dtype in [np.uint16, np.int16]:
            nbytes = 2
        elif layers.dtype in [np.uint32, np.int32]:
            nbytes = 4
        elif layers.dtype in [np.uint32, np.int32]:
            nbytes = 8
        else:
            raise Exception(f"Unexpected dtype of layers ({layers.dtype})")

        classified = layers.view(dtype=(np.void, nlayers*nbytes))[:,:,0]#np.frombuffer(layers, dtype=(np.void, nlayers*4)).reshape(shape[1:])

        feature_attr, feature_attr_idxs = np.unique(classified, return_inverse=True)
        del(classified)
        feature_attr_idxs = feature_attr_idxs.reshape(shape[1:]) + 1 # add 1 so all classes are 'found'

        FinalRASTER = np.zeros(shape, dtype=np.uint32)
        dbg(f"LayerData_export_data() {self.__layer.name} - Found {len(feature_attr)} feature attribute combinations for {self.__layer.name}")

        # change defaultdict to lookup table (in order)
        LUT = dict()
        for attr in self.__RAT_columns:
            keys, vals = [], []
            for key, val in self.__class_LUT[attr].items():
                keys.append(key)
                vals.append(val)
            
            LUT[attr] = np.array(keys)[np.argsort(vals)]

        
        #if nlayers > 1:
        #    import pdb; pdb.set_trace()

        objects = ndimage.find_objects(feature_attr_idxs) 
        for i, slice in enumerate(objects):
            fa = feature_attr[i]
            sel = feature_attr_idxs[slice] == i+1 #remember we added 1 earlier
            cls_idx = self.__all_classes[str(fa)]
            
            FinalRASTER[0][slice][sel] = cls_idx
            
            if not two_step_detection:
                assert nlayers == 1
                LUT_idxs = [self.__class_LUT[attr][np.frombuffer(fa, dtype=layerdtype)[0]]]
            else:
                LUT_idxs = np.frombuffer(fa, dtype=layerdtype)
            
            for iattr, attr in enumerate(self.__RAT_columns):
                val = LUT[attr][LUT_idxs[iattr]]#self.__class_LUT[attr][idxs[iattr]]
               
                if cls_idx == len(self.__RAT_cache[attr]):
                    self.__RAT_cache[attr].append(val)
                elif cls_idx < len(self.__RAT_cache[attr]):
                    if val != self.__RAT_cache[attr][cls_idx]:
                        raise Exception("__RAT_cache out of sync")
                else:
                    raise Exception("__RAT_cache out of sync")

        dbg(f"LayerData_export_data()  {self.__layer.name} - Done")
        return FinalRASTER, self.__RAT_cache


    def LayerData_get_RAT(self):
        return self.__RAT_columns, self.__RAT_cache

    def GET_data(self):
        return self.__data

    def GET_lut(self, colname):
        if colname not in self.__RAT_cache:
            if colname not in self.__RAT_columns:
                dbg(f"Read RAT: {self.__layer.raster} - {colname}")
                lut = rat.readColumn(str(self.__layer.raster), colname)

                if type(lut) is list:
                    lut = np.asarray(lut, dtype='<U')
                elif type(lut) is np.ndarray:
                    if lut.dtype.char == 'S':
                        # need our string arrays to be unicode otherwise comparisons don't work properly later on
                        lut = lut.astype('U')

                self.__RAT_cache[colname] = lut

        return self.__RAT_cache[colname]

    def __getattr__(self, name): 
        if name in self.__dict__:
            return self.__dict__[name]  

        for alternative in self.__alternative_LayerData_sources:
            if alternative.LayerData_attribute_in_external_RAT(name):
                return getattr(alternative, name)

        if name not in self.__layer_attributes: 
            self.__set_layer_attribute_from_RAT(name)

        return self.__layer_attributes[name] 
    
    def __set_layer_attribute_from_RAT(self, name):
        #info(self.__layer.name, self.__data.dtype, self.__data.shape)
        self.__layer_attributes[name] = self.GET_lut(name)[self.__data]
        

def process(luc_name, clip_steps, class_steps, root, layer_manager, tile_idx=None, force=False, tile_overlap=0, wholescene=False, loglevel=logging.DEBUG):

    logger.setLevel(loglevel)
    
    info("Beginning process()")
    ref_img = None
    controls = applier.ApplierControls()
    otherargs = applier.OtherInputs()

    infiles = applier.FilenameAssociations()
    otherargs.thematic_layers = OrderedDict()
    otherargs.aggregate_inputs = []

    info("Inputs:")
    for name, layer in layer_manager.get_input_layers().items():
        info(f"\t{name} - {layer.raster}")

        if name in layer_manager.output_aggregate_record:
            otherargs.thematic_layers[name] = ThematicLayerData(layer=layer, is_input=True)
            otherargs.aggregate_inputs.append(name)

        else:
            otherargs.thematic_layers[name] = ThematicLayerData(layer=layer, is_input=True)
        
        setattr(infiles, name, layer.raster.as_posix())

        if ref_img is None:
            ref_img = layer.raster.as_posix()
            controls.referenceImage = ref_img

    info(f'Ref: {ref_img}')
    imginfo = fileinfo.ImageInfo(ref_img)
    if wholescene:
        warn("WARNING: performing whole image step, monitor mem usage")
        controls.windowxsize = imginfo.ncols
        controls.windowysize = imginfo.nrows
    else:
        imgpx = imginfo.ncols * imginfo.nrows
        if imgpx < 2500 * 2500:
            controls.windowxsize = imginfo.ncols
            controls.windowysize = imginfo.nrows
        else:
            controls.windowxsize = 2500
            controls.windowysize = 2500

        info(f"Using window size {controls.windowxsize}, {controls.windowysize}")


    outfiles = applier.FilenameAssociations()
    output_files = []
    tile_str = '' if tile_idx is None else f'.{tile_idx}'
    ext = None

    info("Outputs:")
    otherargs.aggregate_output = None
    for name, layer in layer_manager.get_output_layers().items():
        info(f"\t{name} {layer} {layer.raster} {type(layer.raster)}")
        
        filename = layer.raster.with_name(
            f'{layer.raster.stem}{tile_str}{layer.raster.suffix}'
        )
        if ext is None:
            ext = filename.suffix.lower()
        elif ext != layer.raster.suffix.lower():
            raise Exception("All outputs must have consistent extensions")
        output_files.append(filename.as_posix())

        if layer.is_thematic:
            otherargs.thematic_layers[name] = ThematicLayerData(layer=layer)

            if layer.name in layer_manager.output_aggregate_record:
                filename = layer.raster.with_name(
                    f'{layer.raster.stem}{tile_str}{layer.raster.suffix}'
                )
                if ext is None:
                    ext = filename.suffix.lower()
                elif ext != layer.raster.suffix.lower():
                    raise Exception("All outputs must have consistent extensions")
            
                info(f"\t{name} - thematic aggregate: {layer.is_thematic} - {filename}")
                
                setattr(outfiles, name, filename.as_posix())
                

                if otherargs.aggregate_output is not None:
                    raise Exception(f"Only one aggregate layer allowed (wanted to add {layer.name} but {otherargs.aggregate_output} already exists)")
                else:
                    otherargs.aggregate_output = layer.name

                controls.setThematic(name, True)
        else:
            info(f"\t{name} - non-thematic: {layer.is_thematic} - {filename}")
            setattr(outfiles, name, filename.as_posix())

    if ext == '.kea':
        controls.drivername = 'KEA'
    elif '.tif' in ext:
        controls.drivername =  'GTiff'
    else:
        raise Exception(f"Invalid extension '{ext}' - currently only KEA and GTiff are supported")
    
    otherargs.clip_steps = clip_steps if len(clip_steps) > 0 else None
    otherargs.class_steps = class_steps
    otherargs.layer_manager = layer_manager
    otherargs.wholescene = wholescene

    controls.calcStats = True

    if tile_overlap:
        controls.overlap = tile_overlap

    info("apply...")
    applier.apply(apply, infiles, outfiles, otherargs, controls=controls)

    info("apply() done, writing RAT")
    columns, RAT = otherargs.thematic_layers[otherargs.aggregate_output].LayerData_get_RAT()
    raster = str(otherargs.layer_manager.output_layers[otherargs.aggregate_output].raster)
    info(f"\t{raster}")
    utils.gdalsetthematic(raster)
    for column in columns:
        #try:
        rat_type, rat_default, rat_data = utils.get_RAT_column_dtype(otherargs.aggregate_output + '.RE', RAT[column])
        #except:
        #    print(column, RAT[column][:10], type(RAT[column][0]), RAT[column].dtype)
        rat_data[0] = rat_default
        rat_data = np.array(rat_data, dtype=rat_type)
        rat.writeColumn(raster, column, rat_data)

    info("process() all done")
    del(otherargs)
    return output_files


def apply(riosinfo, infiles, outfiles, otherargs):

    inputs = Dummy()
    outputs = Dummy()

    inputs.riosinfo = riosinfo

    blksize = riosinfo.getBlockSize()
    blkcoords = riosinfo.getBlockCoordArrays()

    info(f"apply() BLOCK {riosinfo.getBlockCount()} OF {riosinfo.getTotalBlocks()} ({blkcoords[0].min(), blkcoords[1].min(), blkcoords[0].max(), blkcoords[1].max()}")

    inputs.pyluc_steps = otherargs.thematic_layers[otherargs.aggregate_output] #ThematicLayerData(layer='pyluc_steps')

    for name in otherargs.layer_manager.get_input_layers():
        
        if name in otherargs.thematic_layers:
            otherargs.thematic_layers[name].LayerData_load_data(getattr(infiles, name))

            if name in otherargs.aggregate_inputs:
                dbg(f"Aggregate {name} (loading) {getattr(infiles, name).dtype} {getattr(infiles, name)[0,0,0]}")
                inputs.pyluc_steps.LayerData_add_alternative(otherargs.thematic_layers[name])
            else:
                dbg(f"Thematic {name} (loading) {getattr(infiles, name).dtype} {getattr(infiles, name)[0,0,0]}")
                setattr(inputs, name, otherargs.thematic_layers[name])
        else:
            if otherargs.layer_manager.input_layers[name].raster.exists():
                dbg(f"Not Thematic {name}")
                setattr(inputs, name, getattr(infiles, name))
            else:
                dbg(f"Virtual {name} (no action yet)")

    
    #if otherargs.aggregate_output is not None:
    #    inputs.pyluc_steps.LayerData_add_alternative(otherargs.thematic_layers[otherargs.aggregate_output])

    inputs.default = np.ones_like(getattr(infiles, name), dtype=np.bool)
    inputs.valid = inputs.default.copy()

    # build valid mask (if user has supplied clip layers)
    if otherargs.clip_steps:
        for input_name, valid in otherargs.clip_steps.items():
            inputs.valid &= valid(getattr(inputs, input_name))

    for i, step in enumerate(otherargs.class_steps):

        info(f"STEP {i:02d} {step.name}")

        if hasattr(inputs, step.name):
            raise Exception(F"Duplicate input name detected while trying to process step {i} ({step.name})")

        result = step.classify(info, inputs, outfiles, otherargs)

        dbg(f"STEP {i:02d} {step.name} Processed")
        if type(result) is not dict:
            result = {step.name: result}

        for name in result:
            if name not in otherargs.layer_manager.output_attribute_record:
                raise Exception("Returned result not a listed layer")

            data = result[name]
            if len(data.shape) == 2:
                data = data.reshape((1,) + data.shape)
            
            # if name == 'Urb_pc2':
            #     print("URBAN", data[0,885,1252])
            #     otherargs.urbanflag = True

            if name in otherargs.layer_manager.output_layers:
                #info(name, 'non-thematic output')
                setattr(inputs, name, data)
                setattr(outfiles, name, data)
            else:
                dbg(f"setting {name} in pyluc_steps")
                otherargs.thematic_layers[otherargs.aggregate_output].LayerData_load_data(data, set_attribute=name)
  
    otherargs.thematic_layers[otherargs.aggregate_output].LayerData_clear_alternatives(savemem=True)
    aggregate_result, agg_RAT = otherargs.thematic_layers[otherargs.aggregate_output].LayerData_export_data(savemem=True)
    

    setattr(outfiles, otherargs.aggregate_output, aggregate_result)
    
def process_lookup(fin, fout, lut, null_value, thematic):
    
    infiles = applier.FilenameAssociations()
    infiles.fin = fin

    outfiles = applier.FilenameAssociations()
    outfiles.fout = fout

    otherargs = applier.OtherInputs()
    otherargs.lut = lut
    otherargs.null_value = null_value

    controls = applier.ApplierControls()
    controls.thematic = thematic
    controls.calcStats = True
    controls.statsIgnore = null_value
    controls.drivername = 'KEA'

    applier.apply(apply_lookup, infiles, outfiles, otherargs, controls=controls)


def apply_lookup(info, infiles, outfiles, otherargs):

    outfiles.fout = otherargs.lut[infiles.fin]


def raster_is_thematic(file_or_path):

    if type(file_or_path) is not gdal.Dataset:
        if type(file_or_path) is not str:
            file_or_path = str(file_or_path)
        
        file_or_path = gdal.Open(file_or_path, 0)

    return file_or_path.GetMetadataItem('LAYER_TYPE') == 'thematic'
