"""
Created on Mon May  2 15:42:09 2016

@author: jollyb
"""
import os
import re
import glob
import pyproj
import shutil
from pathlib import Path
import json

from collections import OrderedDict, defaultdict

from filelock import FileLock

from koordinates import Client as KClient
from koordinates.exports import Export as KExport

from . import Exceptions
from . import utils

layer, info, dbg, warn = utils.config_logger('pyluc.Layer')

epsg_wgs84 = "EPSG:4326"
epsg_transverse_mercator = "EPSG:2193"
proj_wgs84 = pyproj.Proj(epsg_wgs84)
proj_transverse_mercator = pyproj.Proj(epsg_transverse_mercator)

PREFERRED_RASTER_FORMAT = {
    'gdal_drivername': 'KEA',
    'extension': '.kea'
}

class LayerWorkflow:
    def __init__(self, statefile, ignore_last_state=False):

        self.workflow = utils.Workflow([
                ("init", self._initialise),
                ("marshal", self._marshal),
                ("preprocess", self._preprocess),
                ("rasterise", self._rasterise),
                ("finalise", self._finalise)
            ],
            statefile,
            loadstate = not ignore_last_state,
            loadmeta=True
        )
    
    def ready(self):
        return self.workflow.complete()

    def initialise(self, reprocess_all=False):
        return self.workflow.process(steps="init", force=reprocess_all)

    def marshal(self, reprocess_all=False):
        return self.workflow.process(uptoincl="marshal", force=reprocess_all)

    def preprocess(self, reprocess_all=False):
        return self.workflow.process(uptoincl="preprocess", force=reprocess_all)

    def rasterise(self, reprocess_all=False):
        return self.workflow.process(steps="rasterise", force=reprocess_all)

    def finalise(self, reprocess_all=False):
        return self.workflow.process(force=reprocess_all)

    def _initialise(self, force=False):
        raise NotImplementedError()

    def _marshal(self, statedict, force=False):
        raise NotImplementedError()

    def _preprocess(self, statedict, force=False):
        raise NotImplementedError()

    def _rasterise(self, statedict, force=False):
        raise NotImplementedError()

    def _finalise(self, statedict, force=False):
        raise NotImplementedError()


class LayerManager(LayerWorkflow):

    def __init__(self, extent, auth, project_directory, ignore_last_state):
        self.project_directory = Path(project_directory) if type(project_directory) is str else project_directory
        self.auth = auth
        self.extent = extent

        self.input_layers = OrderedDict()
        self.output_layers = OrderedDict()

        self.output_attribute_record = OrderedDict()
        self.output_aggregate_record = []

        # self.thematic_output = []
        # self.aggregate_output = []
        # self._agg_sublayers = dict()
        # self.output_layers = OrderedDict()
        # self.output_layer_record = []
        #self.continuous_output = OrderedDict()

        self.staged = False
        self.stage_inputs = None
        self.stage_outputs = None

        self.merge_tiled = False

        self.initialised = False

        self.statefile = self.project_directory / '.state'

        super().__init__(self.statefile, ignore_last_state=ignore_last_state)
    
    def set_auth_data(self, auth):
        info(f'setting auth data {auth}')
        self.auth = auth
        for layer in self.input_layers:
            self.input_layers[layer].set_auth_data(auth)

    def set_tile(self, tile_info):
        self.tile_info = tile_info
        for layer in self.input_layers:
            self.input_layers[layer].set_tile(tile_info)
    
    def unset_tile(self):
        self.tile_info = None
        for layer in self.input_layers:
            self.input_layers[layer].unset_tile()

    def stage_wholescene(self, input_layers, output_layers, stage_file):
        self.staged = True

        # deal with input 'output layers' that have been rolled into aggregate 'stage' layers
        self.wholescene_inputs = []
        for name in input_layers:
            if name in self.output_attribute_record:
                name = self.output_attribute_record[name]
                
            self.wholescene_inputs.append(name)

        self.wholescene_inputs = list(set(self.wholescene_inputs))

        self.add_output_layer(stage_file, True, True, self.extent)

        for name in output_layers:
            self.add_output_layer(name, True, False, self.extent, aggregate_parent=stage_file)

        self.wholescene_outputs = output_layers + [stage_file]

    def unstage_wholescene(self):
        self.staged = False
        self.wholescene_inputs = None
        self.wholescene_outputs = None

    def transfer_outputs(self):
        for name in self.output_layers:
            self.input_layers[name] = self.output_layers[name]
            
        self.output_layers = OrderedDict()

    def get_input_layers(self):
        
        if self.staged:
            result = OrderedDict()
            layers_in_result = [] 
            for name in self.wholescene_inputs:
                if name in self.input_layers:
                    result[name] = self.input_layers[name]
                    layers_in_result.append(name)
                elif name in self.output_layers:
                    raise Exception("This shouldn't happen (can't load layers that haven't processed yet)")
                else:
                    output_name = self.output_attribute_record[name]
                    if output_name not in layers_in_result:
                        # this 'input' is the result of a previous stage's 'output' (should have been transferred into inputs via 'transfer_outputs()')
                        result[name] = self.input_layers[output_name]
                        layers_in_result.append(output_name)

            return result
        else:
            return self.input_layers

    def get_output_layers(self):
        if self.staged:
            result = OrderedDict()

            for name in self.wholescene_outputs:
                if name in self.input_layers:
                    raise Exceptions.LUCError("Unexpected error (an output is in an input!)")
                elif name in self.output_layers:
                    result[name] = self.output_layers[name]
                else:
                    pass # this will be a 'sub layer' of an output_file
            return result
        else:
            return self.output_layers

    def add_input_layer(self, name, path_or_url, layer_name=None, clip_layer=False, preprocess=None):
        
        if name in self.input_layers or name in self.output_layers or name in self.output_attribute_record:
            raise Exceptions.LUCError(f"Layer name '{name}' is not unique")

        if type(path_or_url) is str and path_or_url.startswith('http'):
            # currently just assume it's a Koordinates layer
            self.input_layers[name] = KoordinatesLayer(
                name, 
                path_or_url, 
                self.project_directory,
                self.extent,
                preprocess=preprocess
            )
        else:
            # note: this line will still work even if path_or_url is already a Path
            path_or_url = Path(path_or_url).absolute()

            if not path_or_url.exists():
                raise Exceptions.LUCFileNotFoundError(f'Could not find layer {name}: {path_or_url}')
            
            ext = path_or_url.suffix.lower()
            if ext in ['.shp', '.geojson', '.gdb']:
                self.input_layers[name] = LocalVectorLayer(
                    name, 
                    path_or_url, 
                    self.project_directory, 
                    self.extent,
                    preprocess=preprocess,
                    layer_name=layer_name
                )
            elif ext in ['.kea', '.tif']:
                self.input_layers[name] = LocalRasterLayer(
                    name, 
                    path_or_url, 
                    self.project_directory, 
                    self.extent,
                    preprocess=preprocess
                )
            else:
                raise Exceptions.LUCError(f'Unknown file extension {ext}')

    def add_output_layer(self, name, is_thematic, is_aggregate, extent, aggregate_parent=None):
        
        if name in self.input_layers or name in self.output_layers or name in self.output_attribute_record:
            raise Exceptions.LUCError(f"Layer name '{name}' is not unique")

        if aggregate_parent is not None:
            self.output_attribute_record[name] = aggregate_parent
        else:
            raster = name + PREFERRED_RASTER_FORMAT['extension']
            self.output_layers[name] = LocalRasterLayer(
                name, raster, self.project_directory, extent, is_thematic=is_thematic, is_output=True
            )

            statedict = {} #note in the case of output layers we will discard this
            self.output_layers[name]._initialise(statedict)
            #self.output_layers[name]._set_raster(raster)

            # if is_thematic:
            #     self.thematic_output.append(name)
            
            # if is_aggregate:
            #     self._agg_sublayers[name] = []
            #     for sublayer in self.output_layer_record[::-1]:
            #         if sublayer in self.aggregate_output:
            #             break
            #         self._agg_sublayers[name].append(sublayer)

            #     self.aggregate_output.append(name)     

            if is_aggregate:
                self.output_aggregate_record.append(name)
            else:
                self.output_attribute_record[name] = name  

    def ready(self):
        if not self.initialised:
            self._initialise()

        done = 0
        for layer in self.input_layers:
            if self.input_layers[layer].ready():
                done += 1
        return self.workflow.complete() and done >= len(self.input_layers)

    def initialise(self, reprocess_all=False):
        return self.workflow.process(steps="init", force=reprocess_all)

    def marshal(self, reprocess_all=False):
        return self.workflow.process(uptoincl="marshal", force=reprocess_all)

    def preprocess(self, reprocess_all=False):
        return self.workflow.process(uptoincl="preprocess", force=reprocess_all)

    def rasterise(self, reprocess_all=False):
        return self.workflow.process(steps="rasterise", force=reprocess_all)

    def finalise(self, reprocess_all=False):
        return self.workflow.process(force=reprocess_all)

    def _initialise(self, statedict, force=False):
        done = 0
        info('LayerManager._initialise()')
        for layer in self.input_layers:
            if layer in statedict:
                info(f'Restore vars {layer}')
                self.input_layers[layer].restore_vars(statedict[layer])

            if not self.input_layers[layer].ready():
                if self.input_layers[layer].initialise(reprocess_all=force):
                    done += 1
            else:
                done += 1

            statedict[layer] = self.input_layers[layer].get_var_dict()

        self.initialised = done >= len(self.input_layers)
        return self.initialised

    def _marshal(self, statedict, force=False):
        done = 0
        for layer in self.input_layers:
            if not self.input_layers[layer].ready():
                if self.input_layers[layer].marshal(reprocess_all=force):
                    done += 1
            statedict[layer] = self.input_layers[layer].get_var_dict()

        return done >= len(self.input_layers)

    def _preprocess(self, statedict, force=False):
        done = 0
        for layer in self.input_layers:
            if not self.input_layers[layer].ready():
                if self.input_layers[layer].preprocess(reprocess_all=force):
                    done += 1
            statedict[layer] = self.input_layers[layer].get_var_dict()

        return done >= len(self.input_layers)

    def _rasterise(self, statedict, force=False):
        done = 0
        for layer in self.input_layers:
            if not self.input_layers[layer].ready():
                if self.input_layers[layer].rasterise(reprocess_all=force):
                    done += 1
            statedict[layer] = self.input_layers[layer].get_var_dict()
 
        return done >= len(self.input_layers)

    def _finalise(self, statedict, force=False):
        done = 0
        for layer in self.input_layers:
            if not self.input_layers[layer].ready():
                if self.input_layers[layer].finalise(self.merge_tiled, reprocess_all=force):
                    done += 1
            statedict[layer] = self.input_layers[layer].get_var_dict()
        
        return done >= len(self.input_layers)
        

    def marshal_input_layers(self, tile_info=None, merge_tiled=False, reprocess_all=False):
        if tile_info is not None:
            self.set_tile(tile_info)

        self.merge_tiled = merge_tiled

        return self.initialise(reprocess_all=True) and self.finalise(reprocess_all=reprocess_all)
  
class Layer(LayerWorkflow):

    def __init__(self, name, source, project_directory, extent, preprocess=None, layer_name=None, is_thematic=False, is_output=False, source_is_URL=False, ignore_last_state=True):
        self.name = name
        self.source = source

        # used for extracting specific layers from GDBs (for eg)
        self.layer_name = layer_name

        self.source_aux_files = None
        
        self.local_file = None
        self.local_aux_files = None

        self.raster = None
        self.is_thematic = is_thematic
        
        self.extent = extent

        self.tile_info = None
        
        self.project_directory = Path(project_directory).absolute()

        if is_output:
            self.directory = self.project_directory / 'outputs' / self.name
            self.is_output = True
            self.source_file = self.directory / source
            if source_is_URL:
                raise Exceptions.LUCError('Output layers cannot have URLs')
        else:
            if source_is_URL:
                self.source_file = None
            else:
                self.source_file = Path(self.source).absolute()

            self.directory = self.project_directory / 'inputs' / self.name

        self.statefile = self.directory / '.state'

        if preprocess is not None:
            if not callable(preprocess):
                raise Exceptions.LUCUnexpectedDataTypeError(f'preprocess should be a method (got {type(preprocess)})')
            self._preprocess = preprocess

        super().__init__(self.statefile, ignore_last_state=ignore_last_state)

    def get_var_dict(self):
        return {
            'directory': utils.safe_as_posix(self.directory),
            'source_file': utils.safe_as_posix(self.source_file),
            'source_aux_files': utils.safe_as_posix(self.source_aux_files),
            'local_file': utils.safe_as_posix(self.local_file),
            'local_aux_files': utils.safe_as_posix(self.local_aux_files),
            'raster': utils.safe_as_posix(self.raster),
            'tile_info': self.tile_info
        }

    def restore_vars(self, vardict):
        if self.directory.as_posix() != vardict['directory']:
            raise Exceptions.LUCError("State info is for another system (directories do not match)")

        self.source_file = utils.safe_as_Path(vardict['source_file'])
        self.source_aux_files = utils.safe_as_Path(vardict['source_aux_files'])
        self.local_file = utils.safe_as_Path(vardict['local_file'])
        self.local_aux_files = utils.safe_as_Path(vardict['local_aux_files'])
        self.raster = utils.safe_as_Path(vardict['raster'])
        self.tile_info = vardict['tile_info']

        #print('restored raster', self.raster, type(self.raster))

    def _set_source_auxillary_files(self, source_aux_files):
        self.source_aux_files = source_aux_files

    def _set_source_file(self, source_file, source_aux_files=None):
        self.source_file = source_file
        self._set_source_auxillary_files(source_aux_files)

    def _set_local_file(self, local_file, local_aux_files=None):
        self.local_file = local_file
        self.local_aux_files = local_aux_files

    def _set_raster(self, raster):
        self.raster = raster

        if self.tile_info is not None:
            self.raster = self.raster.stem.with_suffix(f".{tile_info['idx']:03d}").with_suffix(self.raster.suffix)

    def set_auth_data(self, auth):
        self.auth = auth
        
    def set_tile(self, tile_info):
        """Set instance to process a 'tile' subset of layer"""
        self.tile_info = tile_info

        # if we're actually doing a tiling operation then change state file (after loading common state)
        if self.tile_info is not None:
            self.statefile = self.directory / f".{self.tile_info['idx']}.state"
            self.statefilelock = FileLock(self.directory / '.state.lock')
            
            # trigger set_raster to update raster file name with tile info
            self._set_raster(self.raster)

            self.save_state()

    def unset_tile(self):
        """Un-do 'set_tile()'"""
        raise NotImplementedError("Not currently supported (assume re-run LUC from new process after tiling finished)")
        self.save_state()
        # take my_raster.00a.xyz and produce my_raster.xyz
        self.raster = self.raster.stem.with_suffix(self.raster.suffix)
        self.tile_info = None
        self.save_state()

    def ready(self):
        return self.workflow.complete()

    def initialise(self, reprocess_all=False):
        return self.workflow.process(steps="init", force=reprocess_all)

    def marshal(self, reprocess_all=False):
        return self.workflow.process(uptoincl="marshal", force=reprocess_all)

    def preprocess(self, reprocess_all=False):
        return self.workflow.process(uptoincl="preprocess", force=reprocess_all)

    def rasterise(self, reprocess_all=False):
        return self.workflow.process(steps="rasterise", force=reprocess_all)

    def finalise(self, merge_tiled=False, reprocess_all=False):
        self.merge_tiled = merge_tiled
        return self.workflow.process(force=reprocess_all)

    def _initialise(self, statedict, force=False):
        
        # build layer dir in project dir
        if not self.project_directory.exists():
            raise Exceptions.LUCFileNotFoundError(f"Project directory {self.project_directory} does not exist")

        if not self.directory.exists():
            if self.tile_info is not None:
                raise Exceptions.LUCError("Layers must be initialised at least once BEFORE tiling")

            self.directory.mkdir(parents=True)

        return True

    def _marshal(self, statedict, force=False):
        """Default behaviour assumes Layer 'source' on local file system

        """

        Exceptions.assert_path_exists(self.source_file)
        tocopy = [self.source_file]
        if self.source_aux_files is not None:
            tocopy += self.source_aux_files
            self.local_aux_files = []

        for i, path in enumerate(tocopy):
            newpath = self.directory / path.name
            info(f'\tcopying {path} to {newpath}...')

            if self.source_file.samefile(path):
                self.local_file = newpath
            else:
                self.local_aux_files.append(path)

            if newpath.exists():
                if force:
                    info('\t\texists, deleting first...')
                    if newpath.is_dir():
                        shutil.rmtree(newpath)
                    else:
                        newpath.unlink()
                else:
                    info('\t\texists, skipping...')
                    continue

            if path.is_dir():
                shutil.copytree(path, newpath)
            else:
                shutil.copy(path, newpath)
            
            info('\t\tdone')

        return True

    def _preprocess(self, statedict, force=False):
        return True

    def _rasterise(self, statedict, force=False):
        return True

    def _finalise(self, statedict, force=False):
        return True

class VectorLayer(Layer):

    def _rasterise(self, statedict, force=False):
        
        raster = self.local_file.with_suffix(PREFERRED_RASTER_FORMAT['extension'])
        if self.layer_name is not None:
            raster = raster.parent / (self.layer_name.replace(' ', '_') + raster.suffix)
        info(f"Set raster {raster}")
        self._set_raster(raster)
        #statedict['raster'] = self.raster

        lockfile = self.raster.with_suffix(self.raster.suffix + '.lock')

        if lockfile.exists():
            info(f"Found lock file {lockfile}")
            return False

        if utils.file_exists_withcontent(self.raster) and not force:
            return True

        cmd = f"pyluc_rasterize.py --all-attr -in {self.local_file} -out {self.raster} " + \
                f" -res {self.extent.res} -ext {' '.join([str(x) for x in self.extent.extent_nztm])} -lck {lockfile}"

        if self.layer_name is not None:
            cmd += f" -layer {self.layer_name}"

        if self.tile_info is not None:
            cmd += f" -tidx {self.tile_info['idx']} -tsize {self.tile_info['size']} {self.tile_info['size']}" + \
                (f" -tolap {self.tile_info['overlap']}" if 'overlap' in self.tile_info and self.tile_info['overlap'] is not None else "")

        # need to spawn lockfile here but rasterize will clean it up
        open(lockfile, 'w').close()
        
        Exceptions.assert_path_exists(self.local_file)

        info(cmd)
        utils.run(cmd, get_std=False, block=True, fail_on_returncode=True)

        #if parallelize:
        #    utils.run(cmd, get_std=False, block=False)
        #    if copyto is not None:
        #        print("WARNING: Can't use copyto with parallelize (ignored)")
        #else:
        #    utils.run(cmd, get_std=False, block=True, fail_on_returncode=True)
        #    if copyto is not None:
        #        utils.run('cp {0} {1}'.format(info.final_raster, copyto), get_std=False, block=True, fail_on_returncode=True)

        return True


class LocalRasterLayer(Layer):

    def _initialise(self, statedict, force=False):
        self._set_raster(utils.safe_as_Path(self.source_file))
        info(f'LocalRasterLayer._initialise() {self.raster} {type(self.raster)}')
        return super()._initialise(statedict, force=force)

    def _marshal(self, statedict, force=False):
        aux = self.source_file.parent.glob(self.source_file.stem + '.*')
        self._set_source_auxillary_files(list(aux))
        return super()._marshal(statedict, force=force)


class LocalVectorLayer(VectorLayer):

    def _marshal(self, statedict, force=False):
        aux = self.source_file.parent.glob(self.source_file.stem + '.*')
        self._set_source_auxillary_files(list(aux))
        return super()._marshal(statedict, force=force)


class KoordinatesLayer(VectorLayer):

    PREFERRED_FORMATS = {
        'grid': [
            'application/x-hdf;subtype=kea',         # KEA
            # 'image/tiff;subtype=geotiff',            # TIFF
            # 'application/x-ogc-aaigrid'              # ASCII
        ],
        'raster': [
            'application/x-hdf;subtype=kea',         # KEA
            # 'image/tiff;subtype=geotiff',            # GeoTIFF
            # 'image/jp2',                             # JPEG2000
            # 'image/jp2;compression=lossless',        # JPEG2000 Lossless
            # 'image/jpeg',                            # JPEG
            # 'image/vnd.dwg',                         # DWG
            # 'application/vnd.google-earth.kml+xml',  # KML
            # 'application/x-erdas-hfa',               # ERDAS Imagine

        ],
        'rat': [
            'application/x-hdf;subtype=kea',         # KEA
            # 'image/tiff;subtype=geotiff',            # GeoTIFF
            # 'application/x-erdas-hfa',               # ERDAS Imagine
        ],
        'table': [
            'application/x-ogc-gpkg',                # GeoPackage
            'application/x-zipped-shp',              # Shapefile
            # 'application/x-ogc-gpkg',                # GeoPackage
            # 'application/x-ogc-mapinfo_file',        # MapInfo TAB
            # 'applicaton/x-ogc-filegdb',              # File Geodatabase
            # 'text/csv'                               # CSV
        ],
        'vector': [
            'application/x-ogc-gpkg',                # GeoPackage
            'application/x-zipped-shp',              # Shapefile
            # 'application/vnd.google-earth.kml+xml',  # KML
            # 'text/csv',                              # CSV
            # 'application/x-ogc-gpkg',                # GeoPackage
            # 'application/x-ogc-mapinfo_file',        # MapInfo TAB
            # 'applicaton/x-ogc-filegdb',              # File Geodatabase
            # 'image/vnd.dwg'                          # DWG
        ]
    }

    PREFERRED_FORMATS_EXT = {
        'grid': [
            'KEA'
        ],
        'raster': [
            'KEA'
        ],
        'rat': [
            'KEA'
        ],
        'table': [
            'GPKG',
            'SHP'
        ],
        'vector': [
            'GPKG',
            'SHP'
        ]
    }

    class Info:
        """Stores information about Koordinates-hosted (ie LRIS) layers
        
        Includes helper functions

        Attributes
        ----------
        _site : str
            Base URL of site
        k_id : int
            Layer ID on site
        name : str
            Layer name on site
        k_type : str
            Type of layer (raster/vector/table)
        export_format : str
            File format of export (normally KEA or SHP)
        directory : pathlib.Path
            Directory where layer will be downloaded/extracted
        expected_zipfile : pathlib.Path
            Compressed file we are expecting from site
        expected_datafile : pathlib.Path
            Main data file we are expecting from compressed file
        final_raster : pathlib.Path
            Final raster to be used for classification
        """

        def __init__(self, k_site, k_id, name, k_type, export_format, directory, expected_zipfile, expected_datafile, final_raster):
            """Populate KoordinatesLayer.Info 

            Parameters
            ----------
            k_site : str
                Base URL of site
            k_id : int
                Layer ID on site
            name : str
                Layer name on site
            k_type : str
                Type of layer (raster/vector/table)
            export_format : str
                File format of export (normally KEA or SHP)
            directory : pathlib.Path
                Directory where layer will be downloaded/extracted
            expected_zipfile : pathlib.Path
                Compressed file we are expecting from site
            expected_datafile : pathlib.Path
                Main data file we are expecting from compressed file
            final_raster : pathlib.Path
                Final raster to be used for classification
            """
            self._site = k_site
            self.k_id = k_id
            self.name = name
            self.k_type = k_type
            self.export_format = export_format
            self.directory = directory
            self.expected_zipfile = expected_zipfile
            self.expected_datafile = expected_datafile
            self.final_raster = final_raster

            self.k_export_id = None

            self.slurm_id = None

        def get_k_client_layer(self, auth):
            """Connect to Koordinates site and get layer information

            Parameters
            ----------
            auth : LUCAuth
                Object with attribute 'ksites' - a dict of objects containing
                attribute 'api_key' (string)

            Returns
            -------
            koordinates.KClient, koordinates.KLayer
                KClient and KLayer objects for querying/downloading
                layer data and metadata from site
            """
            assert self._site in auth.ksites
            kclient = KClient(self._site, auth.ksites[self._site].api_key)

            klayer = kclient.layers.get(id=self.k_id)

            return kclient, klayer

        def change_root(self, new_root):
            """Change directory and expected files to new location"""
            if self.directory.samefile(new_root):
                # nothing to do here
                return

            self.directory = new_root
            self.expected_zipfile = new_root / self.expected_zipfile.name
            self.expected_datafile = new_root / self.expected_datafile.name
            self.final_raster = new_root / self.final_raster.name

        def to_dict(self):
            """Return a JSON-friendly dict of this object (Paths -> strings)
            
            Returns
            -------
            dict
                Dictionary of all properties of this instance
            """
            result = self.__dict__.copy()
            # take Paths and turn them into strings
            for key in ['directory', 'expected_zipfile', 'expected_datafile', 'final_raster']:
                result[key] = str(result[key])
            return result

        @staticmethod
        def from_dict(class_dict):
            """Given a dict, create an instance of KoordinatesLayer.Info

            Parameters
            ----------
            class_dict : dict
                Dictionary containing an entry for each param in __init__
            
            Returns
            -------
            KoordinatesLayer.Info
                Populated instance of this class
            """
            result = KoordinatesLayer.Info(
                class_dict['_site'],
                class_dict['k_id'],
                class_dict['name'],
                class_dict['k_type'],
                class_dict['export_format'],
                Path(class_dict['directory']),
                Path(class_dict['expected_zipfile']),
                Path(class_dict['expected_datafile']),
                Path(class_dict['final_raster'])
            )

            result.k_export_id = class_dict['k_export_id']

            return result

    def __init__(self, name, source, project_directory, extent, preprocess=None):
        self.force_redownload = False
        self.force_reextract = False
        super().__init__(name, source, project_directory, extent, preprocess=preprocess, source_is_URL=True, is_thematic=True)

    def _marshal(self, statedict, force=False):

        self.kstatefile = self.directory / '.kstate'

        marshal_workflow = utils.Workflow([
            ("request", self._marshal_request),
            ("wait", self._marshal_wait),
            ("download", self._marshal_download),
            ("extract", self._marshal_extract)
        ], self.kstatefile)

        # if 'workflow' in statedict:
        #     marshal_workflow.set_state(statedict['workflow'])
        # else:
        #     statedict['workflow'] = marshal_workflow.get_state()

        if 'layer_info' in statedict:
            self.layer_info = KoordinatesLayer.Info.from_dict(statedict['layer_info'])
            self.kclient, self.klayer = self.layer_info.get_k_client_layer(self.auth)
        else:
            self.layer_info, self.kclient, self.klayer = self._populate_layer_info()
            statedict['layer_info'] = self.layer_info.to_dict()

        self._set_local_file(self.layer_info.expected_datafile)
        self._set_raster(self.layer_info.final_raster)

        self.path = self.layer_info
        #marshal_workflow.save_state()


        if self.layer_info.expected_zipfile.exists():
            if self.force_redownload:
                for fp in self.layer_info.directory.glob('*'):
                    fp.unlink()

        if self.layer_info.expected_datafile.exists():
            if self.force_reextract:
                for fp in self.layer_info.directory.glob('*'):
                    if fp.suffix not in ['.zip']:
                        fp.unlink()

        marshal_workflow.process()

        #statedict['layer_info'] = self.layer_info.to_dict()
        #marshal_workflow.save_state()

        return marshal_workflow.complete()

    def _marshal_request(self, statedict, force=False):
        if self.layer_info.expected_zipfile.exists():
            return True

        info('\tCreating export for ' + self.layer_info.name)
        #print(self.extent.wgs84_bounding_box)
        info(f'{self.extent.nztm_bounding_box}')
        kexport = KExport(
            crs=epsg_transverse_mercator, #epsg_wgs84,
            formats={self.layer_info.k_type: self.layer_info.export_format},
            extent=self.extent.wgs84_bounding_box
        )

        kexport.add_item(self.klayer)

        info('\tChecking export is valid...')
        kvalid = self.kclient.exports.validate(kexport)
        if kvalid.is_valid:
            info('\t\tTrue')
        else:
            info('\t\tFalse: ' + kvalid.invalid_reasons)

        #if not check_only:
        info('\tSubmitting export')
        self.layer_info.k_export_id = self.kclient.exports.create(kexport).id

        statedict['layer_info'] = self.layer_info.to_dict()

        return True
    
    def _marshal_wait(self, statedict, force=False):
        if self.layer_info.expected_zipfile.exists():
            return True

        info('\tChecking export for {0} ({1})'.format(self.layer_info.name, self.layer_info.k_export_id))
        kexport = self.kclient.exports.get(self.layer_info.k_export_id)
        kexport.refresh()
        info('\t{0} ({1:0.2f}%)'.format(kexport.state, (kexport.progress * 100.0) if kexport.progress is not None else 0))
        if kexport.state in ['complete']:
            return True
        elif kexport.state in ['processing']:
            return False
        elif kexport.state in ['gone', 'cancelled']:
            info('\t\tWill retry on next call...')
            self.layer_info.k_export_id = None
            statedict['layer_info'] = self.layer_info.to_dict()
            return None
        elif kexport.state in ['error']:
            raise Exceptions.LUCError('Koordinates site export failed with status \'error\'')
        else:
            raise Exceptions.LUCError('Unknown Koordinates site layer download status {0}'.format(kexport.state))

        raise Exceptions.LUCError('Reached unobtainable state (_marshal_wait)!')

    def _marshal_download(self, statedict, force=False):
        if self.layer_info.expected_zipfile.exists():
            return True

        info('Downloading export ' + self.layer_info.name)
        kexport = self.kclient.exports.get(self.layer_info.k_export_id)
        kexport.download(self.layer_info.expected_zipfile)
        Exceptions.assert_path_exists(self.layer_info.expected_zipfile)

        return True

    def _marshal_extract(self, statedict, force=False):
        if self.layer_info.expected_datafile.exists():
            return True

        info('Unzipping export ' + self.layer_info.name)
        Exceptions.assert_path_exists(self.layer_info.expected_zipfile)
        os.system(f'unzip -o {self.layer_info.expected_zipfile} -d {self.layer_info.directory}')
        Exceptions.assert_path_exists(self.layer_info.expected_datafile)
        info('\tDone')

        return True

    def _populate_layer_info(self):

        url_chunks = re.split('/+', self.source.strip('/'))

        site, layer_components = url_chunks[1], url_chunks[-1].split('-')
        layer_id = int(layer_components[0])

        kclient = KClient(site, self.auth.ksites[site].api_key)

        klayer = kclient.layers.get(id=layer_id)

        # if user only specified layer number in URL (not full URL) then we need
        #  to grab the full URL to generate our layer name
        if len(layer_components) == 1:
            layer_components = re.split('/+', klayer.url_html.strip('/'))[-1].split('-')

        layer_name = '-'.join(layer_components[1:])


        if site not in self.auth.ksites:
            raise Exceptions.LUCError('Layer site {0} not in API keys provided'.format(site))

        #import pdb; pdb.set_trace()

        available_formats = kclient.exports.get_formats()
        layer_ext = None
        layer_export_format = None

        for i, preferred_format in enumerate(KoordinatesLayer.PREFERRED_FORMATS[klayer.kind]):
            if preferred_format in available_formats[klayer.kind]:
                layer_ext = KoordinatesLayer.PREFERRED_FORMATS_EXT[klayer.kind][i]
                layer_export_format = preferred_format
                break

        assert layer_ext is not None
        assert layer_export_format is not None

        info(f"Creating {layer_export_format} export {site} {layer_id}")
        layer_info = KoordinatesLayer.Info(
            site,
            layer_id,                           # k_id,
            layer_name,                         # name,
            klayer.kind,                        # k_type,
            layer_export_format,                # export_format,
            self.directory,                     # directory,
            self.directory / f"{layer_name}-{layer_ext.upper()}.zip",     # expected_zipfile,
            self.directory / f"{layer_name}.{layer_ext.lower()}",         # expected_datafile,
            self.directory / f"{layer_name}{PREFERRED_RASTER_FORMAT['extension']}",       # final_raster
        )

        return layer_info, kclient, klayer

    def rasterize_layers(self, statedict, force=False):

        if self.layer_info.k_type not in ['table', 'vector']:
            self._set_raster(self.layer_info.final_raster)

            lockfile = self.raster.with_suffix(self.raster.suffix + '.lock')

            if lockfile.exists():
                info('Found lock file {0}'.format(lockfile))
                return False

            if self.layer_info.final_raster.exists() and not force:
                return True

            #cmd = 'gdal_translate -of kea -tr {res} {res} -projwin {ext} -stats -ot UInt32 {in} {out}'.format({
            #        'in': info.expected_datafile,
            #        'out': info.final_raster,
            #        'res': self.extent.res,
            #        'ext': ' '.join([str(x) for x in self.extent.extent_nztm])
            #    })
            raise NotImplementedError("Raster Koordinates layers not currently supported")

            # need to spawn lockfile here but rasterize will clean it up
            open(lockfile, 'w').close()
            
            Exceptions.assert_path_exists(self.local_file)

            utils.run(cmd, get_std=False, block=True, fail_on_returncode=True)

            #if parallelize:
            #    utils.run(cmd, get_std=False, block=False)
            #    if copyto is not None:
            #        print("WARNING: Can't use copyto with parallelize (ignored)")
            #else:
            #    utils.run(cmd, get_std=False, block=True, fail_on_returncode=True)
            #    if copyto is not None:
            #        utils.run('cp {0} {1}'.format(info.final_raster, copyto), get_std=False, block=True, fail_on_returncode=True)

            return True
        else:
            return super()._rasterise(statedict, force=force)

    def get_layer_zipfiles(self):
        return [info.expected_zipfile for info in self.layer_info.values()]