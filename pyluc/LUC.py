"""
Created on Mon Apr 11 09:50:55 2016

@author: jollyb
"""
import os
import sys
import math
import re
import json
import logging

from pathlib import Path

import collections

from . import LUCMetadata
from . import Layer
from . import ClassificationStepFactory
from . import RIOSEngine
from . import Exceptions
from . import utils
from . import OutputManager
from . import Provenance
from . import Documentation
from . import LUCAuth

logger, info, dbg, warn = utils.config_logger('pyluc.LUC')

def LRIS_LAYERID_CHECK(url):
    """Check for 'old' LRIS layers IDs and change

    Required for backwards compatibility after Koordinates moved LRIS to the
    cloud, should not affect other sites
    """
    url_chunks = re.match(r'^(.*lris\.scinfo\.org\.nz/.*/)(\d+)([\w\-]*/?)$', url)
    if url_chunks is not None:
        layer_id = int(url_chunks.group(2))

        if layer_id < 48000:
            layer_id += 48000
            new_url = '{0}{1}{2}'.format(url_chunks.group(1), layer_id, url_chunks.group(3))
            print('WARNING: Old LRIS layer id detected, updating url ({0} -> {1})'.format(url, new_url))
            url = new_url

    return url


class Classification():
    """
    This class is designed to handle all steps of the LUC process while keeping
    the calling code easy to write for novice programmers

    Attributes
    ----------
    name : str
        Name of the LUC.
    durectory : str
        Directory we will be operating within
    output_basename : str
        Start of output file name ('.*step*.kea' will be appended for each
        classification step)
    """

    def __init__(self, name, extent, resolution, directory=None, tile_size_m=40e3, tile_overlap=0, auth=None):
        """Create a landuse classification.

        This will handle all steps of the classification process.

        Parameters
        ----------
        name : str
            Name of the LUC

        Attributes
        ----------
        name : str
            Name of this Classification
        directory : str
            Directory containing data files (and probably but not necessarily
            the LUC definition script)
        output_basename : str
            Base component, including path, of all output file names

        Returns
        -------
        Classification
            An engine for managing the LUC process

        """

        if directory is None:
            directory = Path(sys.argv[0]).absolute().parent

        self.name = name

        self.directory = None
        self.output_basename = None
        self._nonvolatile_state = None

        self.set_processing_directory(directory, update_layers=False)

        #self._inputs = []
        self._class_steps_staged = [[]]
        self._class_steps_istage = 0
        #self._class_steps = []
        self._clip_steps = dict()

        self._user_info = utils.get_user_info()

        self._tile_size = tile_size_m
        self._tile_overlap = tile_overlap

        if type(extent) is not LUCMetadata.ClassificationExtent:
            self._extent = LUCMetadata.ClassificationExtent(extent, resolution)
        else:
            self._extent = extent

        self.auth = auth
        # self._auth_file = os.path.join(self.directory, 'auth.json')
        # api_keys = None
        # provstore_auth = None
        # if os.path.exists(self._auth_file):
        #     print('Loading auth info from {0} ...'.format(self._auth_file))

        #     with open(self._auth_file) as handle:
        #         auth = json.load(handle)

        #     if 'api_keys' in auth:
        #         api_keys = auth['api_keys']

        #     if 'provstore_auth' in auth:
        #         provstore_auth = auth['provstore_auth']

        # else:
        #     print('WARNING: Auth data file not found (tried {0})'.format(self._auth_file))

        # if api_keys is None:
        #     print("\tWARNING: LRIS API keys not specified (won't affect validation but will affect classification)")

        # if provstore_auth is None:
        #     print("\tWARNING: ProvStore API key not specified (non-fatal - only affects upload of PROV data)")

        self._layer_manager = Layer.LayerManager(self._extent, self.auth, self.directory, True)

        self._state_loaded = False

        self._proc_steps = Classification.ProcessingSteps(self)

        self._provenance = Provenance.LUCProvenance(name, auth=self.auth)
        self.doc = Documentation.LUCDocumentation(name, self._provenance)
        # self.metadata = LUCMetadata.LUCProvMetaHelper(self._provenance)

        self.vectorize = False

        self.loglevel = logging.DEBUG

    def set_loglevel(self, loglevel):
        self.loglevel = loglevel
        logger.setLevel(self.loglevel)

    def vectorize_final_output(self):
        """Specify that final output raster should be vectorized
        """
        self.vectorize = True

    def set_processing_directory(self, directory, update_layers=True):
        """Helper method to set the working directory of this class

        Parameters
        ----------
        directory : str
            (Preferably) absolute path to processing directory where all data
            files should reside (input and output)
        update_layers : bool (optional)
            Do we need to update any layers that have already been added? Must
            be 'True' if input layers have already been added
        """

        self.directory = utils.enforce_Path(directory)
        self.output_basename = self.directory / self.name
        self._nonvolatile_state = self.output_basename.with_suffix('.state')

        if update_layers:
            self._proc_steps._load_state()
            self._layer_manager.change_directory(self.directory)

    def add_clip_layer(self, name, path_or_url, attributed_to=None, varname='FID+1', valid=lambda fid: fid > 0):
        """Add a 'clip' layer to the LUC from a Koordinates (i.e. LRIS) URL.

        An clip layer is a column from a shapefile or band from a raster. It is
        OK to use this function several times for a single data source (URL) in
        order to add several layers. Clip layers are used to clip all output
        layers and are applied as a 'last step' after all processing has
        completed. All clip layers are combined using a 'logical AND' operation
        to produce a single mask.

        Parameters
        ----------
        name : str
            Unique name to use for accessing this dataset during the LUC process
        varname : str
            The name of the column (shapefile) or band (raster) to use during
            the classification, if multiple values are required then
            'add_clip_layer' needs to be called once for each value
        path_or_url : str
            Path to a dataset on LRIS or similar Koordinates site (i.e. LINZ)
        valid : lambda expression
            lambda expression used to define a 'valid' region in the clip layer
            (if you're just providing a poly layer like the NZ Coastlines just
            leave this parameter as default). The expression should take one
            parameter (numpy array) and return a numpy array (simple logical
            expressions like x > 0 will work just fine without modification)
        """
        self._proc_steps._check_operator_added()
        self.add_input_layer(name, varname, path_or_url, attributed_to=attributed_to, clip_layer=True)
        self._clip_steps[name] = valid

    def add_input_layer(self, name, path_or_url, layer_name=None, attributed_to=None, clip_layer=False):
        """Add an input layer to the LUC from a Koordinates (i.e. LRIS) URL.

        An input layer is a shapefile or a raster. It is
        OK to use this function several times for a single data source (URL) in
        order to add several layers, however the 'add_input_layers_bulk'
        function is a cleaner way of doing this.

        Parameters
        ----------
        name : str
            Unique name to use for accessing this dataset during the LUC process
        path_or_url : str
            Path to a dataset on disk or LRIS or similar Koordinates site (i.e. LINZ)
        clip_layer : bool (optional)
            If specified, tell the Provenance tracker this is a 'clip' layer
        """
        self._proc_steps._check_operator_added()

        if name in self._layer_manager.input_layers:
            raise Exceptions.LUCError("Layer name must be unique, already have {}".format(name))

        lyr_url = LRIS_LAYERID_CHECK(path_or_url)
        self._layer_manager.add_input_layer(name, path_or_url, layer_name=layer_name, clip_layer=clip_layer)
        #self._inputs.append(name)
        self.doc._add_input_layer(name, path_or_url, attributed_to=attributed_to, clip_layer=clip_layer, layer_name=layer_name)

    def add_classification_step(self, authors, name, lut_or_method, input_name=None, attribute=None, default_val=None):
        """Add a classification step to the LUC.

        Parameters
        ----------
        authors : str OR list(str) OR tuple(str)
            Author(s) of this step, as already added using doc.add_luc_author
        name : str
            Name to call the results so they can be accessed by later steps.
        lut_or_method : list(tuple) OR dict OR callable
            Either a LUT (list of tuples: [(input_val, 'class')] OR a 'dict')
            OR a callable function that takes a single parameter and returns a
            LUT, e.g.
            def myfunc(infiles):
                result = [
                    ('Urban', infiles.lcdb2012 == 1),
                    ('Pasture', infiles.lcdb2012 == 15)
                ]
                return result
        input_name : str
            Name of the input dataset to use (must match the name of either an
            input file or a previous classification step).

        Raises
        ------
        LUCUnexpectedDataTypeError
            If the there's a problem with one of the parameters
        """
        self._proc_steps._check_operator_added()

        self._class_steps_staged[self._class_steps_istage].append(ClassificationStepFactory.create_classification_step(name, lut_or_method, input_name, attribute, isthematic=True, default_val=default_val))

        self.doc._add_classification_step(authors, name, lut_or_method, input_name, attribute)

    def copy_input_attributes(self, authors, input_layer, attributes):
        """Copy attributes from input layer directly to output

        This is equivalent to adding a 'fake' classification step that just
        copies input data.

        Parameters
        ----------
        input_layer : str
            The name of the input layer to copy the attributes from
        attributes : str OR list(str) OR dict(str:str)
            Attribute(s) to copy with optional rename (k,v pairs in dict)

        Raises
        ------
        LUCUnexpectedDataTypeError
            If the there's a problem with one of the parameters
        """
        isdict = False
        if type(attributes) is str:
            attributes = [attributes]
        elif isinstance(attributes, collections.Mapping):
            isdict = True
        
        for attr in attributes:
            if isdict:
                name = attributes[attr]
            else:
                name = attr
            
            self.add_classification_step(authors, name, None, input_layer, attr)


    def add_wholescene_classification_step(self, authors, output_names, method, input_names):
        """Add a 'whole scene' classification step to the LUC.

        This will cause all processing to halt and all intermediate layers will
        be written to disk as one large thematic raster. This will then be re-
        opened it is entirety and presented to the classification method, other
        layers will not be opened so make sure anything you need for this step
        is either the result of a classification step or copied from the
        appropriate input layer using copy_input_variable.

        Parameters
        ----------
        authors : str OR list(str) OR tuple(str)
            Author(s) of this step, as already added using doc.add_luc_author
        name_s : str OR list(str)
            Name(s) of output layers that will be created - as this kind of 
            step is potentially very computationally expensive we need to 
            enable multiple outputs
        method : callable
            A callable function

        Raises
        ------
        LUCUnexpectedDataTypeError
            If the there's a problem with one of the parameters
        """
        self._proc_steps._check_operator_added()

        if type(output_names) is str:
            output_names = [output_names]

        if type(input_names) is str:
            input_names = [input_names]

        if len(self._class_steps_staged[self._class_steps_istage]) > 0:
            self._class_steps_staged.append([])
            self._class_steps_istage += 1

        self._class_steps_staged[self._class_steps_istage] = {
            'step': ClassificationStepFactory.create_classification_step(output_names[0], method, wholescene=True),
            'inputs' : input_names,
            'outputs': output_names
        }

        self._class_steps_staged.append([])
        self._class_steps_istage += 1

        #self.doc._add_classification_step(authors, name, lut_or_method, input_name)

    def set_auth_data(self, auth):
        self.auth = auth
        self._layer_manager.set_auth_data(self.auth)

    def get_tile_info(self, idx):
        """Get number of tiles (x, y, total)

        Based on the tile size provided during initialisation, return the number
        of tiles required to complete the classification (if tiling is used)

        Returns
        -------
        tuple(int, int, int)
            Number of tiles in x and y dimensions, as well the total (nx * ny)
        """

        xmin, ymin, xmax, ymax = self._extent.extent_nztm
        xsize, ysize = xmax - xmin, ymax - ymin

        nx, ny = math.ceil(xsize / self._tile_size), math.ceil(ysize / self._tile_size)

        return {
            'idx': idx,
            'size': self._tile_size,
            'overlap': self._tile_overlap,
            'nx': nx,
            'ny': ny,
            'ntiles': nx * ny
        }

    def get_input_files(self):
        """Get list of all layer zip filenames, plus state and auth files.

        Useful for transferring everything to a temporary directory for
        processing

        Returns
        -------
        list(str...)
            List of all path to all EXPECTED zip files (files may not yet exist
            if Layer Manager is still waiting to download them), then the full
            path to the state JSON file (filenames[-2]), then the full path to the
            Koordinates auth file used for this run (filenames[-1])
        """

        self._proc_steps._load_state()

        return self._layer_manager.get_layer_zipfiles() + [self._nonvolatile_state, self._auth_file]

    def generate_provenance(self, upload=False):

        provn = self._provenance.get_prov_string()

        with open(self.directory / f"{self.name}_provenance.provn", 'w') as handle:
            handle.write(provn)

        if upload:
            self._provenance.upload_to_provstore()

    def generate_latex_documentation(self):

        latex = self.doc.get_latex()

        with open(self.directory / f"{self.name}_doc.tex", 'w') as handle:
            handle.write(latex)

    class ProcessingSteps():
        """
        Class to keep processing methods separate from init methods (if user has
        autocomplete this will simplify things for them by compartmentalising
        things they don't necessarily need to see)
        """

        def __init__(self, classification):
            """Initialise ProcessingSteps with pointer to current Classification

            Parameters
            ----------
            classification : Classification
                Instance of Classification that we will be performing steps on
            """

            self.c = classification

        def acquire_input_data(self, force):
            """Acquire and marshall all input data into consistent formats

            Request and download data sets from Koordinates sites. Remembers
            previous state and checks for existing downloads so can be called
            multiple times without necessarily duplicating efforts.

            Parameters
            ----------
            force : bool
                Force a re-download and re-extraction of input zip files if
                they already exist in the file system (will not force
                Koordinates to re-create the download request)

            Returns
            -------
            bool
                True if step completed successfully, else False if still
                processing

            Raises
            ------
            pyluc.Exceptions.LUCKError
                If there is an error requsting or downloading input data from
                a Koordinates site
            pyluc.Exceptions.LUCFileNotFoundError
                If a file is not found when one was expected (multiple checks
                throughout acquisition process) - usually indicative of another
                error (check stdout/stderr)
            """
            self._load_state()

            result = self.c._layer_manager.marshal_input_layers(tile_info=None, merge_tiled=False, reprocess_all=force)

            # save state
            self._save_state('acquire')

            # if result is 'true' then we can go to next state
            return result

        def process_input_data(self, force, tile_idx=None, start_stage=0):
            """After acquiring input data, apply classification rules

            Only call this AFTER acquire_input_data() returns True.
            Rasterize vector inputs or resample and reproject raster inputs as
            required before applying classification rules to produce output
            rasters. This can optionally be done on a tile-by-tile basis. This
            step covers a lot of I/O intensive operations so it is advisable to
            perform it using a temporary RAM disk if using an HPC environment
            (this is why it's so large as it makes sense for a single 'job' to
            process the whole lot).
            Output can be vectorised after this step during merge_results() if
            vector data is required.

            Parameters
            ----------
            force : bool
                Force re-rasterization and re-processing if files already exist
            tile_idx : int, optional
                If not None, process specified tile - x/y indices are found by:
                    ix, iy = tile_idx % num_x, tile_idx // num_x
            Returns
            -------
            bool
                True when step completed successfully, never returns False
                (step blocks until complete)

            Raises
            ------
            pyluc.Exceptions.LUCRasterizeError
                If something went wrong during rasterization
                (check stdout/stderr)
            """

            self._load_state()

            # assert self.c._layer_manager.download_layers(force_redownload=False, force_reextract=force), "Data didn't exist (call acquire_input_data() again)"

            tile_info = self.c.get_tile_info(tile_idx) if tile_idx is not None else None

            rasterize_success = self.c._layer_manager.marshal_input_layers(
                tile_info=tile_info,
                merge_tiled=False,
                reprocess_all=force
            )

            if not rasterize_success:
                raise Exceptions.LUCRasterizeError("Unknown problem occurred during rasterization, check stdout/stderr")

            for istage, stage in enumerate(self.c._class_steps_staged):

                steps = stage
                stage_name = f"{self.c.name}_Stage{istage+1:02d}"

                if type(stage) is dict:
                    steps = [stage['step']]

                    self.c._layer_manager.stage_wholescene(stage['inputs'], stage['outputs'], stage_name)
                else:
                    
                    self.c._layer_manager.add_output_layer(
                        stage_name, 
                        True, 
                        True,
                        self.c._extent
                    )

                    for step in steps:
                        self.c._layer_manager.add_output_layer(
                            step.name, 
                            step.isthematic, 
                            False,
                            self.c._extent,
                            aggregate_parent=stage_name if step.isthematic else None
                        )

                if istage >= start_stage:
                    RIOSEngine.process(
                        self.c.name, 
                        self.c._clip_steps, steps,#self.c._class_steps, 
                        self.c.directory, 
                        self.c._layer_manager, 
                        tile_idx=tile_idx, force=force, tile_overlap=0, wholescene=self.c._layer_manager.staged,
                        loglevel=self.c.loglevel
                    )

                if self.c._layer_manager.staged:
                    self.c._layer_manager.unstage_wholescene()

                self.c._layer_manager.transfer_outputs()

                self._save_state('process')

            return True

        def merge_results(self, force, istep=None, temp_dir=None):
            """After processing, merge as necessary then vectorize if req'd

            This is important if tiling was used to process large extents as it
            collects all tiles into a single raster for each output. If tiling
            was not used then this step could still be important if vectors are
            required

            Parameters
            ----------
            force : bool
                Force re-vectorization and re-merging if files already exist
            istep : int, optional
                Can be used to specify which output to merge/vectorize (useful
                for parallelising this step)
            temp_dir : str (optional)
                Transfer files to this temp directory for processing if req'd
            Returns
            -------
            bool
                True when step completed successfully, never returns False
                (step blocks until complete)
            """
            step = None
            if istep is not None:
                step = self.c._class_steps[istep].name

            # output_manager = OutputManager.OutputManager(self.c.directory, self.c._layer_manager, self.c.output_basename, temp_dir)
            # output_manager.merge_and_vectorize(force, step, vectorize=self.c.vectorize)

            self._save_state('merge')

            return True

        def get_previous_step(self):
            result = None
            if utils.file_exists_withcontent(self.c._nonvolatile_state):
                with open(self.c._nonvolatile_state, 'r') as handle:
                    state = json.load(handle)
                    if 'step' in state:
                        result = state['step']
            
            return result

        def _save_state(self, step):
            with open(self.c._nonvolatile_state, 'w') as handle:
                state = {
                    'step': step,
                    #'input_layer_manager_state': self.c._layer_manager.get_state()
                }
                json.dump(state, handle, default=lambda o: o.__dict__)

        def _load_state(self, force=False):
            if force:
                # this shouldn't be necessary (only need to skip elif statement) but better safe than sorry
                pass #self.c._layer_manager.purge_state()
            elif not self.c._state_loaded:
                if utils.file_exists_withcontent(self.c._nonvolatile_state):
                    with open(self.c._nonvolatile_state, 'r') as handle:
                        state = json.load(handle)
                        #self.c._layer_manager.load_state(state['input_layer_manager_state'])
                    self.c._state_loaded = True
                    return True
            else:
                return False

        def _check_operator_added(self):
            if self.c.doc.operator_username is None:
                shortname, fullname = self.c._user_info
                affiliation = self.c.doc.owner_organisation
                print('WARNING: LUC operator not specified, automatically adding:')
                print('\tUser name:', shortname)
                print('\tFull name:', fullname)
                print('\tAffiliatn:', affiliation)
                print('\tDelegatn :', 'None')
                self.c.doc.add_luc_operator(shortname, fullname, affiliation)
            elif self.c.doc.operator_username != self.c._user_info[0]:
                raise Exceptions.LUCError('Manually-specified LUC operator ({0}) does not match current user ({1})'.format(self.c.doc.operator_username, self.c._user_info[0]))
