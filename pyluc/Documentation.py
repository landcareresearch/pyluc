import re
from . import utils


class Organisation(object):
    
    def __init__(self, shortname, name, url, luc_owner):
        self.shortname = shortname
        self.name = name
        self.url = url
        self.luc_owner = luc_owner


class Person(object):
    
    def __init__(self, shortname, fullname, affiliation, luc_author, luc_operator, delegators):
        self.shortname = shortname
        self.fullname = fullname
        
        if type(affiliation) is not Organisation:
            raise TypeError()
            
        self.affiliation = affiliation
        
        self.luc_author = luc_author
        self.luc_operator = luc_operator
        
        self.delegators = delegators


class Input(object):
    
    def __init__(self, name, path_or_url, attributed_to, clip_layer):
        self.name = name
        #self.varname = varname
        self.path_or_url = path_or_url
        
        if type(attributed_to) is not Organisation:
            raise TypeError()
            
        self.attributed_to = attributed_to
        self.clip_layer = clip_layer
    

class Rule(object):
    
    def __init__(self, step_name, function_name, function_code, uses_inputs, authors):
        self.step_name = step_name
        self.function_name = function_name
        self.function_code = function_code
        if type(uses_inputs) is not list:
            raise TypeError()
        self.uses_inputs = uses_inputs
        self.authors = authors
        

class LUCDocumentation():
    
    def __init__(self, name, prov):
        
        self.name = name
        self.prov = prov
        
        self.orgs = dict()
        self.orgs_order = []
        
        self.people = dict()
        self.people_order = []

        self.inputs = dict()
        self.inputs_order = []

        self.rules = dict()
        self.rules_order = []

        self.owner_organisation = None
        self.operator_username = None

        self.inline_lambda_count = 1
        self.inline_var_count = 1

    def _add_org(self, shortname, name, url, luc_owner):
        self.orgs[shortname] = Organisation(shortname, name, url, luc_owner)
        self.orgs_order.append(shortname)
        
        self.prov.add_organization(shortname, name, url, is_luc_owner=luc_owner)
        if luc_owner:
            self.owner_organisation = shortname
        
    def _add_person(self, shortname, fullname, affiliation, luc_author, luc_operator, delegators):
        if type(delegators) is not list:
            delegators = [delegators] if delegators is not None else []

        self.people[shortname] = Person(shortname, fullname, self.orgs[affiliation], luc_author, luc_operator, [self.people[delegator] for delegator in delegators])
        self.people_order.append(shortname)
        
        self.prov.add_person(shortname, fullname, affiliation, is_luc_author=luc_author, is_luc_operator=luc_operator)
        if luc_operator:
            self.operator_username = shortname
        
    def _add_input_layer(self, name, path_or_url, attributed_to, clip_layer, layer_name=None):
        self.inputs[name] = Input(name, path_or_url, self.orgs[attributed_to], clip_layer)
        self.inputs_order.append(name)
        self.prov.add_input_layer(name, path_or_url, attributed_to=attributed_to, clip_layer=clip_layer)
        
    def _add_rule(self, step_name, function_name, function_code, uses_inputs, authors):
        if type(authors) is not list:
            authors = [authors]

        self.rules[step_name] = Rule(step_name, function_name, function_code, uses_inputs, [self.people[author] for author in authors])
        self.rules_order.append(step_name)
        
        self.prov.add_rule(step_name, function_name, uses_inputs, authors)
        
    def _add_classification_step(self, authors, name, lut_or_method, input_name, attribute=None):
        
        lom_name, lom_decl = None, None
        
        if lut_or_method is None:
            lom_name, lom_decl = f'copy_{input_name}_{attribute}_to_{name}', 'getattr(getattr(infiles, input_layer), attribute)'
        else:
            lom_name, lom_decl = utils.get_var_source(lut_or_method, 2, 'lut_or_method')

            if lom_name == lom_decl:
                if 'lambda' in lom_name:
                    lom_name = f'inline_lambda{self.inline_lambda_count}'
                    self.inline_lambda_count += 1
                else:
                    lom_name = f'inline_var{self.inline_var_count}'
                    self.inline_var_count += 1
        
            
        # TODO: this probably belongs in utils
        if input_name is not None:
            if attribute is not None:
                inputs = [f"{input_name}.{attribute}"]
            else:
                inputs = [input_name]
        else:
            assert callable(lut_or_method)
            assert lut_or_method.__code__.co_argcount == 1, 'Complex methods not yet supported for PROV'
            
            inputs_name = lut_or_method.__code__.co_varnames[0]
            inputs = re.findall(inputs_name + r'\.(\w+[\.\w+])', lom_decl)
            inputs = list(set(inputs))

            if len(inputs) == 0:
                # try to match the getattr lambda for copying attributes
                inputs = re.findall(f'getattr\\(getattr\\({inputs_name},\\s*(\\w+)\\),\\s*(\\w+)\\)', lom_decl)
                inputs = ['.'.join(x) for x in inputs]

            assert len(inputs) > 0
            
            
            if 'default' in inputs:
                inputs.remove('default')
                
        if type(lut_or_method) is dict:
            lom_decl = lom_decl.replace('{', '{\n\t').replace(' ,', ',').replace(',', ',\n\t').replace('}', '\n}')
                
        self._add_rule(name, lom_name, lom_decl, inputs, authors)
        
    def get_latex(self):
        # \begin{minted}{python}
        _base = r"""
        \documentclass{article}
        \usepackage[utf8]{inputenc}
        \usepackage{booktabs}
        \usepackage{minted}
        \usepackage[landscape]{geometry}
        \PassOptionsToPackage{hyphens}{url}\usepackage{hyperref}
        \begin{document}
        
        **title**
        
        \section{Organisations}
        **organisations**
        
        \section{People}
        **people**
        
        \section{Inputs}
        **inputs**
        
        \section{Rules}
        **rules**
        
        \end{document}
        """

        def url(x):
            return '\\url{' + x + '}'
        
        title = r"""
        \title{""" + self.name + r""" LUC Automated Documentation}
        \author{""" + self.people[self.operator_username].fullname + " [" + self.operator_username + '] (operator), ' + self.people[self.operator_username].affiliation.name + r"""}
        \maketitle
        """.replace('_', r'\_')
        
        orgs = r"""
        \begin{tabular}{llp{10cm}l}
            Short Name & Full Name & URL & LUC Owner \\ \midrule
        """ + '\\\\ \n'.join(
            [' & '.join([self.orgs[org].shortname, self.orgs[org].name, url(self.orgs[org].url), str(self.orgs[org].luc_owner)]) for org in self.orgs_order]
        ).replace('_', r'\_') + r"""
        \end{tabular}
        """
            
        people = r"""
        \begin{tabular}{llllll}
            Short Name & Full Name & Affiliation & LUC Author & LUC Operator & Delegators \\ \midrule
        """ + '\\\\ \n'.join(
            [' & '.join([self.people[p].shortname, self.people[p].fullname, self.people[p].affiliation.name, str(self.people[p].luc_author), str(self.people[p].luc_operator), ', '.join([dele.fullname for dele in self.people[p].delegators])]) for p in self.people_order]
        ).replace('_', r'\_') + r"""
        \end{tabular}
        """
        
        inputs = r"""
        \begin{tabular}{llp{10cm}ll}
            Name & Field & Data Location & Attributed To & Clipping Layer \\ \midrule
        """ + '\\\\ \n'.join(
            [' & '.join([self.inputs[i].name, self.inputs[i].varname, url(self.inputs[i].path_or_url), self.inputs[i].attributed_to.name, str(self.inputs[i].clip_layer)]) for i in self.inputs_order]
        ).replace('_', r'\_') + r"""
        \end{tabular}
        """
        
        rules = r"""
        \subsection{Simple Table}
        \begin{tabular}{llll}
            Step Name & Function/LUT Name & Inputs Used & Author(s) \\ \midrule
        """ + '\\\\ \n'.join(
            [' & '.join([self.rules[r].step_name, self.rules[r].function_name, ', '.join(self.rules[r].uses_inputs), ', '.join([author.fullname for author in self.rules[r].authors])]) for r in self.rules_order]
        ).replace('_', r'\_') + r"""
        \end{tabular}
        \subsection{Rule Definitions}
        """ + "\n".join([
            r"\subsubsection{" + ' - '.join([self.rules[r].step_name, self.rules[r].function_name]).replace('_', r'\_') + "}\\begin{minted}[linenos]{python}\n" + self.rules[r].function_code + "\n\\end{minted}\n"
            for r in self.rules_order
        ])
        
        return _base.replace('**title**', title).replace('**organisations**', orgs).replace('**people**', people).replace('**inputs**', inputs).replace('**rules**', rules)

    def add_luc_owner(self, shortname, name, url):
        self._add_org(shortname, name, url, True)
    
    def add_other_organization(self, shortname, name, url):
        self._add_org(shortname, name, url, False)
    
    def add_luc_author(self, shortname, fullname, affiliation, luc_operator=False):
        if luc_operator:
            self.add_luc_operator(shortname, fullname, affiliation, luc_author=True)
        else:
            self._add_person(shortname, fullname, affiliation, True, luc_operator, None)
    
    def add_luc_operator(self, shortname, fullname, affiliation, delegators=None, luc_author=False):
        self._add_person(shortname, fullname, affiliation, luc_author, True, delegators)
    
    def add_other_person(self, shortname, fullname, affiliation, delegators=None):
        self._add_person(shortname, fullname, affiliation, False, False, delegators)
