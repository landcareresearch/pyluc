import json

class Auth():

    class AuthData():
        def __init__(self, site=None, user=None, api_key=None, pw=None):
            self.site = site
            self.user = user
            self.api_key = api_key
            self.pw = pw

    def __init__(self, name=None, json_file=None):
        self.name = name
        self.ksites = {}
        self.provstore = None
        self.ftpsites = {}

        if json_file is not None:
            with open(json_file, 'r') as handle:
                auth = json.load(handle)
                for key, data in auth.items():
                    if key == 'name':
                        self.name = auth['name']
                    elif key  == 'ksites':
                        for site, api_key in data.items():
                            self.add_ksite(site, api_key)
                    elif key == 'provstore':
                        self.add_provstore(data['user'], data['api_key'])
                    elif key == 'ftp':
                        for site, info in data.items():
                            self.add_ftp(site, info['user'], info['pw'])

    def add_ksite(self, site, api_key):
        self.ksites[site] = Auth.AuthData(site=site, api_key=api_key)
    
    def add_provstore(self, user, api_key):
        self.provstore = Auth.AuthData(user=user, api_key=api_key)

    def add_ftp(self, site, user, pw):
        self.ftpsites[site] = Auth.AuthData(site=site, user=user, pw=pw)
