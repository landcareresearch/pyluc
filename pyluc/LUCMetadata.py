"""
Created on Mon Apr 18 15:59:59 2016

@author: jollyb
"""

from pyproj import Transformer

default_extent = (1084000, 6234000, 2092000, 4722000)

regions = {
    'NZ': (1084000, 6234000, 2092000, 4722000)
}

proj_nztm_to_wgs84 = Transformer.from_crs("EPSG:2193", "EPSG:4326")
proj_wgs84_to_nztm = Transformer.from_crs("EPSG:4326", "EPSG:2193")


class ClassificationExtent(object):

    def __init__(self, extent, resolution):

        if type(extent) is str:
            extent = regions[extent]

        #proj_wgs84 = pyproj.Proj("EPSG:4326")
        #proj_nztm = pyproj.Proj("EPSG:2193")
        

        if extent[0] < 91:
            self.extent_nztm = proj_wgs84_to_nztm.transform(extent[1::2], extent[::2])
        else:
            self.extent_nztm = extent[::2], extent[1::2]

        self.extent_nztm = min(self.extent_nztm[0]), min(self.extent_nztm[1]), max(self.extent_nztm[0]), max(self.extent_nztm[1])

        buffered_nztm = self.extent_nztm[0] - 10000, self.extent_nztm[1] - 10000, self.extent_nztm[2] + 10000, self.extent_nztm[3] + 10000

        self.nztm_corners = (
            (buffered_nztm[0], buffered_nztm[1]),
            (buffered_nztm[0], buffered_nztm[3]),
            (buffered_nztm[2], buffered_nztm[3]),
            (buffered_nztm[2], buffered_nztm[1])
        )

        self.wgs84_corners = tuple(proj_nztm_to_wgs84.transform(y, x) for x, y in self.nztm_corners)

        # self.wgs84_bounding_box = {
        #     "type": "MultiPolygon",
        #     "coordinates": [[[
        #         [self.wgs84_corners[0][0], self.wgs84_corners[0][1]],
        #         [self.wgs84_corners[1][0], self.wgs84_corners[1][1]],
        #         [self.wgs84_corners[2][0], self.wgs84_corners[2][1]],
        #         [self.wgs84_corners[3][0], self.wgs84_corners[3][1]],
        #         [self.wgs84_corners[0][0], self.wgs84_corners[0][1]]
        #     ]]]
        # }

        self.wgs84_bounding_box = {
            "type": "MultiPolygon",
            "coordinates": [[[
                [self.wgs84_corners[0][1], self.wgs84_corners[0][0]],
                [self.wgs84_corners[1][1], self.wgs84_corners[1][0]],
                [self.wgs84_corners[2][1], self.wgs84_corners[2][0]],
                [self.wgs84_corners[3][1], self.wgs84_corners[3][0]],
                [self.wgs84_corners[0][1], self.wgs84_corners[0][0]]
            ]]]
        }

        self.nztm_bounding_box = {
            "type": "MultiPolygon",
            "coordinates": [[[
                [self.nztm_corners[0][0], self.nztm_corners[0][1]],
                [self.nztm_corners[1][0], self.nztm_corners[1][1]],
                [self.nztm_corners[2][0], self.nztm_corners[2][1]],
                [self.nztm_corners[3][0], self.nztm_corners[3][1]],
                [self.nztm_corners[0][0], self.nztm_corners[0][1]]
            ]]]
        }

        self.res = resolution


class LUCProvMetaHelper():

    def __init__(self, provenance_instance):
        self.prov = provenance_instance
        self.owner_organisation = None
        self.operator_username = None

    def add_luc_owner(self, shortname, name, url):
        owner = self.prov.add_organization(shortname, name, url, is_luc_owner=True)
        self.owner_organisation = shortname
        return owner

    def add_other_organization(self, shortname, name, url):
        return self.prov.add_organization(shortname, name, url)

    def add_luc_author(self, shortname, fullname, affiliation):
        return self.prov.add_person(shortname, fullname, affiliation, is_luc_author=True)

    def add_luc_operator(self, shortname, fullname, affiliation, delegators=None):
        operator = self.prov.add_person(shortname, fullname, affiliation, is_luc_operator=True, delegators=delegators)
        self.operator_username = shortname
        return operator

    def add_other_person(self, shortname, fullname, affiliation, delegators=None):
        return self.prov.add_person(shortname, fullname, affiliation, delegators=delegators)
