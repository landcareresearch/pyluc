"""
Created on Mon Jun 27 08:43:49 2016

@author: jollyb
"""
import os
import glob

from . import utils


class OutputManager():

    def __init__(self, directory, class_steps, output_basefile, save_intermediate, temp_dir=None):

        self.directory = directory

        self.filenames = dict()
        self.output_basefile = output_basefile
        self.temp_dir = temp_dir

        self.save_intermediate = save_intermediate

        self.final = None

        steps_to_proc = class_steps if save_intermediate else [class_steps[-1]]

        for step in steps_to_proc:
            # note this call to 'doramdisk' won't copy files (shouldn't exist yet), just changes file name
            self.filenames[step.name] = '{0}.{1}.kea'.format(self.output_basefile, step.name)
            self.final = step.name

    def merge_and_vectorize(self, force=False, step=None, vectorize=False):

        def _genkwargs(stepname, fn, **kwargs):
            basefn = os.path.splitext(fn)[0]

            kwargs['keafile'] = fn
            kwargs['keatiles'] = ' '.join(glob.glob(basefn + '.*.kea'))
            print(os.getcwd(), basefn + '.*.kea')
            kwargs['shpfile'] = basefn + '.shp'
            kwargs['attribute'] = stepname

            return kwargs

        def _proc(stepname, fn, vectorize=False):
            kwargs = _genkwargs(stepname, fn, temp_dir=self.temp_dir)
            orig_kwargs = None

            if self.temp_dir is not None:
                orig_kwargs = kwargs
                utils.run(
                    'pyluc_tmpdir.py {temp_dir} {keafile} {keatiles}'.format(**orig_kwargs),
                    get_std=False, block=True, fail_on_returncode=True
                )
                tmpfn = os.path.join(self.temp_dir, os.path.basename(fn))
                kwargs = _genkwargs(stepname, tmpfn, temp_dir=self.temp_dir)

            # merge
            if len(kwargs['keatiles']) == 0:
                print('No KEA files found to merge')
            else:
                utils.run(
                    'pyluc_rastermerge.py {keafile} {attribute} {keatiles}'.format(**kwargs),
                    get_std=False, fail_on_returncode=True
                )

                # copy merged kea files back now in case something goes wrong with vectorize
                if self.temp_dir is not None:
                    utils.run(
                        'pyluc_tmpdir.py {temp_dir} {keafile} {keatiles} --reverse --reverseoriginals'.format(**orig_kwargs),
                        get_std=False, block=True, fail_on_returncode=True
                    )

            # vectorize
            if vectorize:
                utils.run(
                    'pyluc_vectorize.py {keafile} {attribute} {shpfile}'.format(**kwargs),
                    get_std=False, block=True, fail_on_returncode=True
                )

                # copy shapefile back (add keafile to list of 'originals' so it's ignored)
                if self.temp_dir is not None:
                    utils.run(
                        'pyluc_tmpdir.py {temp_dir} {keafile} {keatiles} --reverse'.format(**orig_kwargs),
                        get_std=False, block=True, fail_on_returncode=True
                    )

        if (step is None) and (not self.save_intermediate):
            step = self.final

        if step is None:
            for stepname, fn in self.filenames.items():
                _proc(stepname, fn, vectorize=vectorize and stepname == self.final)
        else:
            stepname, fn = step, self.filenames[step]
            _proc(stepname, fn, vectorize=vectorize and stepname == self.final)
