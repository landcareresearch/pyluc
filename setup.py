from distutils.core import setup

setup(
    name = "pyluc",
    packages = ["pyluc"],
    scripts = [
        "tools/pyluc_process.py",
        "tools/pyluc_rastermerge.py",
        "tools/pyluc_vectorize.py",
        "tools/pyluc_rasterize.py",
        "tools/pyluc_tmpdir.py",
    ],
    version = "1.0.0",
    description = "Python land use classification engine",
    author = "Ben Jolly",
    author_email = "jollyb@landcareresearch.co.nz",
    url = "https://bitbucket.org/landcareresearch/pyluc",
    download_url = "",
    keywords = ["landuse", "classification", "landcare"],
    classifiers = [
        "Programming Language :: Python :: 3",
        "Development Status :: 4 - Beta",
        "Environment :: Other Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
    long_description = """\
This version requires Python 3.6 or later.
"""
)
