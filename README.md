# pyluc #

## Install Instructions (conda) ##


```
conda create -n pyluc -c conda-forge rios requests prov pyproj scipy
conda activate pyluc
pip install provstore-api koordinates filelock

cd ../..
git clone git clone git@bitbucket.org:landcareresearch/pyluc.git
cd pyluc
python setup.py install
```

## Authentication/API keys

There are two ways of providing auth keys:
 1: put an 'auth.py' file at the same level as your luc script
 ```python
from pyluc import LUCAuth

my_auth = LUCAuth('User Name')

# Koordinates site API keys
my_auth.add_ksite('lris.scinfo.org.nz', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
my_auth.add_ksite('data.linz.govt.nz', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')

my_auth.add_provstore('username', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')

my_auth.add_ftp('ftp.mysite.com', user='user.name', password='password123')

 ```
 2: as 1, but 'auth.json'
 ```json
{
    "name": "User Name",
    "ksites": {
        "lris.scinfo.org.nz": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
        "data.linz.govt.nz": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    },
    "provstore": {
        "user": "username",
        "api_key": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    },
    "ftp" : {
        "ftp.mysite.com": {
            "user": "user.name", 
            "password": "password123"
        }
    }
}
 ```


## Run Instructions ##


```
cd my_project_directory
pyluc_process.py my_script.py
```

